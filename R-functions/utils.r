###########################################################################################################
# PLOTS the final partition of BNP-MRF model, whatever study area under consideration 
############################################################################################################
#########################################################################################
# Default set of colors
#########################################################################################

default_color_number = 15
default_color_list = c("grey66", "yellow3", "red", "green", "orange", "pink", "blue", "blue4", "grey33", "green4", "antiquewhite4", "lightseagreen", "chocolate4", "cornflowerblue", "orchid4")

default_colors <- function(k=default_color_number){
# Return k colors 
    if (k<=default_color_number) {
        cols = default_color_list} else {
        set.seed(0)
        cols = colors()
        cols = sample(cols)
        cols = setdiff(cols, default_color_list)
        cols = c(default_color_list, cols)}
    return(cols[1:k])
}

#########################################################################################
# Display marginal entropies of labels
#########################################################################################

# ent_file: name of file containing entropy values
# map_object: map as an instance of a spatialPolygonsDataFrame
# ndp: number of decimal places
# lcex: point size for legends

plot_entropy <- function(ent_file, map_object, lpos="topleft", decimal.mark=".", ndp=3, 
                         cex=0.7, lcex=0.7, ...){
    if (decimal.mark==".") {
        Sys.setlocale("LC_NUMERIC","C")
        options(OutDec=".")
    }
    H = unlist(read.csv(ent_file, header = FALSE))
    if(max(H) != 0){
      cols <-  grey(1-H/max(H))
    }else{
      cols <- grey(1-H)
    }
    plot(map_object, col=cols, main="Marginal state entropy", cex=cex, ...)
    lg = format(round(seq(0,max(H),length.out=5), ndp), decreasing = FALSE, scientific = FALSE,
                digits = ndp + 1)
    legend(lpos, legend=lg, fill=grey(seq(1,0,length.out=5)), cex=lcex)
}

compute_ratio <- function(data_file, write=""){
  # Compute ratio of case counts on population counts
  D = read.csv(data_file, header = TRUE)
  n = dim(D)[1] # number of vertices
  C = D[,1] # cases
  P = D[,2] # population
  R = rep(1, n)
  R[P>0] = C[P>0] / P[P>0]  # ratio
  M = max(R)
  # Threshold inf ratio to the maximum observed value
  R[P==0] = M # R is not necessarily <= 1 
  if (nchar(write) > 0) {
    write.table(R, write)}
  return(cbind(R, P, C))
}

plot_ratio <- function(data_file, map_object, mycolors=NULL, lpos="topleft", 
                       decimal.mark=".", ndp=3, cex=0.7, lcex=0.7, detail=FALSE, ...){
  # Plot 3 maps: population counts, case counts and ratio
  # mycolors: choice of colors to use instead of grey levels.
  # ndp: number of decimal places  
  # detail: provide mean number of cases in legend, standard deviations and range
  # lcex: point size for legends  
  
  # Each variable is binned 
  if (decimal.mark==".") {
    Sys.setlocale("LC_NUMERIC","C")
    options(OutDec=".")
  }
  RPC = compute_ratio(data_file)
  R = RPC[,1] # ratio
  P = RPC[,2] # population
  C = RPC[,3] # counts
  n = dim(RPC)[1] # number of vertices
  M = max(max(C[P>0] / P[P>0]), 1)
  m = 0
  if (m == M) { 
    cols = grey(rep(1,n)) } else {
      # create linear grey scale
      v = 1 # Color of min
      V = 0 # Color of max
      cols = rep(0, n)
      # Plot population
      a = (V-v) / (M-m)
      b = V - a * M
      cols = a * R + b
      cols[cols < 0] = 0
      cols[cols > 1] = 1      
      cols = grey(cols)}
  if (is.null(mycolors)) {
    # Use grey levels
    plot(map_object, col=cols, main="Case ratio", cex=cex, ...)
    lg = format(round(seq(0, M, length.out=8), ndp), decreasing = FALSE, scientific = FALSE,
                digits = ndp + 1)                
    legend(lpos, legend=lg, fill=grey(seq(1,0,length.out=8)))} else {
      # Use colormap
      nb.cols = length(mycolors)
      max.color = length(mycolors)
      # Create bins on R, start from 0, end at M, create max.color bins 
      breaks = c(0,quantile(R[R>0], seq(1,max.color-1)/max.color), M)
      nbreaks = length(breaks)
      mylabels = rep(0, length((R)))
      # R == 0 -> label 1 since breaks[0] == 0
      for (i in 1:(nbreaks-2)) {
        mylabels[(R >= breaks[i]) & (R < breaks[i+1])] = i
        stopifnot(sum(mylabels==nbreaks)==0)
      }
      mylabels[R >= breaks[i+1]] = max.color
      stopifnot(all(mylabels > 0))
      # If R <=1 set last bin max range to 1
      breaks[nbreaks] = max(max(R), 1)
      # Transform quantiles to colors
      # Min label is 1 -> argument map_cluster_to_color(mylabels-1, ...)
      cols = map_cluster_to_color(mylabels-1, mycolors)
      plot(map_object, col=cols, main="Case ratio", ...)
      # legend (breaks for bins in ratio)
      lg.base = format(round(breaks[1:(nbreaks-1)], ndp), decreasing = FALSE, scientific = FALSE, digits = ndp + 1)
      lg = lg.base
      if (detail) {
        # Add mean, sd and range per cluster      
        means = rep(0, nbreaks)
        sd = rep(0, nbreaks)
        r.inf = rep(0, nbreaks)
        r.sup = rep(0, nbreaks)}
      i = 1
      if (detail) {
        means[i] = format(round(mean(C[R >= breaks[i] & R <= breaks[i+1]]), ndp))
        sd[i] = format(round(sd(C[R >= breaks[i] & R <= breaks[i+1]]), ndp))
        r.inf[i] = min(C[R >= breaks[i] & R <= breaks[i+1]])
        r.sup[i] = max(C[R >= breaks[i] & R <= breaks[i+1]])
        lg[i] = paste(lg.base[i], "m:", means[i], "s:", sd[i], "rng:", r.inf[i], ",", r.sup[i]) }
      else {lg[i] = lg.base[i]}
      for (i in 2:(nbreaks-2)) {
        if (detail) {
            means[i] = format(round(mean(C[R > breaks[i] & R <= breaks[i+1]]), ndp))
            sd[i] = format(round(sd(C[R > breaks[i] & R <= breaks[i+1]]), ndp))
            r.inf[i] = min(C[R > breaks[i] & R <= breaks[i+1]])
            r.sup[i] = max(C[R > breaks[i] & R <= breaks[i+1]])
            lg[i] = paste(lg.base[i], "m:", means[i], "s:", sd[i], "rng:", r.inf[i], ",", r.sup[i])}
        else {lg[i] = lg.base[i]}}
      i = nbreaks-1 # last break: also write bin upper bound
      if (detail) {      
        means[i] = format(round(mean(C[R > breaks[i] & R <= breaks[i+1]]), ndp))
        sd[i] = format(round(sd(C[R > breaks[i] & R <= breaks[i+1]]), ndp))
        r.inf[i] = min(C[R > breaks[i] & R <= breaks[i+1]])
        r.sup[i] = max(C[R > breaks[i] & R <= breaks[i+1]])
        lg[i] = paste(lg.base[i], "-", format(round(breaks[nbreaks], ndp)), "m:", means[i], "s:", sd[i], "rng:", r.inf[i], ",", r.sup[i])} 
      else {lg[i] = paste(lg.base[i], "-", format(round(breaks[nbreaks], ndp)))}        
      
      legend(lpos, legend=lg, fill=mycolors[1:length(breaks)], cex=lcex)}
}

plot_cases <- function(data_file, map_object, rescale_counts=FALSE, mycolors=NULL, lpos="topleft", 
                       decimal.mark=".", ndp=3, cex=0.7, lcex=0.7, detail=FALSE, ...){
  # Plot number of individuals, cases and ratio
  # rescale_counts: Handle 0 / 1 counts with separate colours / levels of grey.
  # mycolors: choice of colors to use instead of grey levels.
  # ndp: number of decimal places
  # detail: provide mean number of cases in legend, standard deviations and range
  # lcex: point size for legends  

  if (decimal.mark==".") {
    Sys.setlocale("LC_NUMERIC","C")
    options(OutDec=".")
  }
  D = read.csv(data_file, header = TRUE)
  n = dim(D)[1] # number of vertices
  C = D[,1] # cases
  P = D[,2] # population
  # Population counts
  M = max(P)
  m = min(P)
  par(mfrow=c(1,3))
  if (m == M) { cols = grey(rep(1,n)) }
  if ((m != M) && (rescale_counts)) {
    m = min(P[P>1])
    v = 1-2/256 # Color of min > 1
    V = 0 # Color of max
    # N.B (by default 0 is black, 1 is white)
    cols = rep(0, n)
    # Plot population
    cols[P==0] = grey(1)
    cols[P==1] = grey(1-1/256)
    a = (V-v) / (M-m)
    b = V - a * M
    cols[P>1] = grey(a * P[P>1] + b)}
  if ((m != M) && !(rescale_counts)) {
    v = 1 # Color of min
    V = 0 # Color of max
    cols = rep(0, n)
    # Plot population
    a = (V-v) / (M-m)
    b = V - a * M
    cols = a * P + b
    cols[cols < 0] = 0
    cols[cols > 1] = 1
    cols = grey(cols)}
  par(mfg=c(1,1)) 
  plot(map_object, col=cols, main="Population counts", cex=cex, ...)
  lg = format(round(seq(0, M, length.out=8), ndp), decreasing = FALSE, scientific = FALSE, 
              digits = ndp + 1, cex=lcex)
  legend(lpos, legend=lg, fill=grey(seq(1,0,length.out=8)), cex=lcex)
  # Case counts
  M = max(C)
  m = min(C)
  if (m == M) { cols = grey(rep(1,n)) }
  if ((m != M) && (rescale_counts)) {
    m = min(C[C>1])
    v = 1-2/256 # Color of min > 1
    V = 0 # Color of max
    # N.B (by default 0 is black, 1 is white)
    cols = rep(0, n)
    # Plot population
    cols[C==0] = grey(1)
    cols[C==1] = grey(1-1/256)
    a = (V-v) / (M-m)
    b = V - a * M
    cols[C>1] = grey(a * C[C>1] + b)}
  if ((m != M) && !(rescale_counts)) {
    v = 1 # Color of min
    V = 0 # Color of max
    cols = rep(0, n)
    # Plot population
    a = (V-v) / (M-m)
    b = V - a * M
    cols = a * C + b
    cols[cols < 0] = 0
    cols[cols > 1] = 1
    cols = grey(cols)}   
  if (decimal.mark==".") {
    Sys.setlocale("LC_NUMERIC","C")
    options(OutDec=".")
  }  
  par(mfg=c(1,2)) 
  plot(map_object, col=cols, main="Case counts", cex=cex, ...)
  l.o = min(M+1, 8)
  lg = round(seq(0, M, length.out=l.o)) # integers: no ndp here
  legend(lpos, legend=lg, fill=grey(seq(1,0,length.out=l.o)), cex=lcex) 
  # Plot ratio    
  par(mfg=c(1,3)) 
  plot_ratio(data_file, map_object, mycolors, lpos, decimal.mark=decimal.mark, ndp=ndp, 
             cex=cex, lcex=lcex, detail=detail, ...)  
}

map_cluster_to_color<- function(labels, colors){
  # The min possible label is assumed to be 0
  # Give each label a color. Labels may be replicated 
  # and may not have successive values
    my_labels = labels + 1
    if (length(colors) < length(unique(my_labels))){
    print("error the number of colors is insufficient !!!!!!!!!!!!!!!!!!!!!!!!!!!!")

    }

    i = 1 # label of color
    label_set = sort(unique(my_labels))
    for (i in 1:length(label_set)){
        e = label_set[i]
        my_labels[my_labels == e] = colors[i]
    }
    my_labels
}

#########################################################################################
# Graphical representation of clusters
#########################################################################################

# map_object: map as an instance of a spatialPolygonsDataFrame
# labels_file: name of file containing labels
# risks_file: name of file containing risks (e.g., risk estimates)
# ndp: number of decimal places
plot_clusters <- function(map_object, labels_file, risks_file, lpos="topleft", 
                          decimal.mark=".", ndp=3, cex=0.7, lcex=0.7, mycolors=NULL, ...){

  # mycolors: choice of colors to use instead of grey levels.
  # ndp: number of decimal places
  
  if (decimal.mark==".") {
    Sys.setlocale("LC_NUMERIC","C")
    options(OutDec=".")
  }  
  # load data
  risks = read.csv(risks_file, header = FALSE)
  labels = unlist(read.csv(labels_file, header = FALSE))

  # number of clusters
  k = length(risks)
  if (is.null(mycolors)) {
    mycolors = default_colors(max(k, default_color_number))}
  
  
  # color and legend
  cols = map_cluster_to_color(labels, mycolors)
  # Identify existing labels and add 1 to them (begin at 1)
  label.set = unique(labels)
  aux = sort(label.set) + 1
  # legend
  lg = format(risks[aux, 1], scientific = TRUE, digits = ndp+1)
  for (i in 1:length(lg)) {
    state = aux[i]
    lg[i] = paste("state", state-1, lg[i])
  }
  
  size = length(cols)
  if (size < 1264){
      cols <- c(cols, rep("white", 1264-size))
  }
  # plot
  plot(map_object, col=cols, cex=cex, ...)
  # legend(lpos, legend=lg, fill=mycolors[aux], cex=cex)
  legend(lpos, legend=lg, fill=mycolors[1:length(label.set)], cex=lcex)
}


#########################################################################################
# Format any study area (e.g. Kentucky) data
#########################################################################################

# Extract and save the spatial structure of a study area (neighborhood structure) from the 
# corresponding spatialPolygonsDataFrame
# "spatial_structure" is a graph whose nodes are the regions and edges represent adjacent regions.
# NB: Nodes are numbered from 1 to the number of regions.
SpatialPolygonsDataFrame_to_neighborsMatrix <- function(spatial_structure, output_file){
  
    # number of polygons (regions) in the map "spatial_structure"
    n <- length(spatial_structure@polygons)
    
    # transform "spatial_structure" into neighbors matrix or adjacency matrix, tmp is sparse.
    tmp <- poly2nb(spatial_structure)

    # dregree of the first node in the graph
    m_degree = length(unlist(tmp[1]))
  
    # seek for the highest degree in the graph 
    for (e in seq(2,n)){
  
      if (length(unlist(tmp[e])) > m_degree)
    
        m_degree = length(unlist(tmp[e])) 
  }

  # Construct and file the full adjacency matrix. m[i] stores the list of the indeces of the neighbors of i_th region.
  # If length(m[i]) < m_degree, m[i] is complemented by -1 in such a way that length(m[i]) = m_degree. 
  # Therefore, m is a matrix n x m_degree.
    
  print(m_degree)
  m <- matrix(nrow = n, ncol = m_degree)
  for (i in seq(n)){
    vois <- unlist(tmp[i])
    m[i,] <- c(vois-1, rep(-1, m_degree-length(vois))) # vois-1 because indices start from 0 in python 
  }

  write.csv(m, file = output_file, row.names = FALSE)
}


