# Correspondence analysis

corresp = function(ContTab, title="", lpos="topleft", var1.name=NULL, var2.name=NULL, svg.path=NULL, ...)
{
    # Correspondence analysis
    # tile: text to be added to titles
    # lpos: position of legend
    # var1.name = name of variable 1
    # var2.name = name of variable 2
    # svg.path = path to save .svg files
    
    Label1 = row.names(ContTab)
    Label2 = names(ContTab)
    n = sum(ContTab)    

    Freq = as.matrix(ContTab / n)
    l = nrow(ContTab)
    c = ncol(ContTab)
    # row barycenter = cols frequencies
    Gl = t(rep.int(1, nrow(Freq)) %*% Freq)
    # cols baryecenter = rows frequencies 
    Gc = Freq %*% rep.int(1, ncol(Freq))

    Ind = Gc %*% t(Gl)
    Dist.Ind = (Freq - Ind) / Ind


    # matrices for weights and metrics
    Dc = diag(c(Gl), nrow=c)
    Dl = diag(c(Gc), nrow=l)
    Dcm1 = diag(c(1/Gl), nrow=c)
    Dlm1 = diag(c(1/Gc), nrow=l)

    # tables of rows & cols profiles
    Tl = Dlm1 %*% Freq
    Tc = Freq %*% Dcm1

    # matrices whose eigenvalues are required
    Kc = t(Tl) %*% Tc
    Jl = Tc %*% t(Tl)
    Eleml = eigen(Kc)
    Elemc = eigen(Jl)

    # Ignore eigenvalue 1
    vpmax = min(l, c) - 1
    Uc = Re(Elemc$vectors[, 2:(vpmax+1)])
    vp = Re(Eleml$values[2:(vpmax+1)])
    Lambda = Re(diag(c(vp), nrow=vpmax))

    TotInertia = sum(vp)

    # percentage of representation on each principal axis
    for(i in 1:vpmax)
    {
            print(c(i, vp[i], 100 * vp[i]/TotInertia, 100 * sum(vp[1:i])/TotInertia), digits=2)
    }
    # normalize spanning vectors
    for(i in 1:vpmax)
    {
            Uc[, i] = Uc[, i] / sqrt(t(Uc[, i]) %*% Dlm1 %*% Uc[, i])
    }
    Ul = t(Tl) %*% Uc %*% diag(c(1/sqrt(vp))) 

    # principal components
    Cc = t(Tc) %*% Dlm1 %*% Uc
    Cl = Tl %*% Cc %*% diag(c(1/sqrt(vp))) 
    # = Tl %*% Dcm1 %*% Ul

    # percentage of representation of categories


    rows_quality = Cl
    row.names(rows_quality) = Label1
    for(i in 1:l)
            rows_quality[i, ] = Cl[i, ]^2 / sum(Cl[i, ]^2)*100

    for(i in 1:l)
    {
            cat("Quality of representation for individual ", Label1[i], " on ", c, " axes : \n")
            print(rows_quality[i, ])
    }

    contrib_rows = rows_quality
    for(i in 1:l) contrib_rows[i,] = Dl[i,i] * Cl[i,]^2 / vp

    for(i in 1:l)
    {
            cat("Contribution of individual ", Label1[i], " to", c," axes : \n")
            print(contrib_rows[i, ]*100)
    }

    cols_quality = Cc
    row.names(cols_quality) = Label2
    for(i in 1:c)
            cols_quality[i, ] = Cc[i, ]^2 / sum(Cc[i, ]^2)*100

    for(i in 1:c)
    {
            cat("Quality of representation for individual ", Label2[i], " on ", l, " axes : \n")
            print(cols_quality[i, ])
    }

    contrib_cols = cols_quality
    for(i in 1:c) contrib_cols[i,] = Dc[i,i] * Cc[i,]^2 / vp

    for(i in 1:c)
    {
            cat("Contribution of individual ", Label2[i], " to", l," axes : \n")
            print(contrib_cols[i, ]*100)
    }

    DI.c = Dcm1 %*% Ul %*% diag(c(sqrt(sqrt(vp))))
    DI.l = Dlm1 %*% Uc %*% diag(c(sqrt(sqrt(vp))))

    # Representation with double PCA
    # postscript("acp_double.eps", horizontal=F, onefile=F) 
    main1 = paste("Double PCA:", title)
    main2 = paste("CA Biplot:", title)    
    
    if (is.null(var1.name)) var1.name = "Row var."
    if (is.null(var2.name)) var2.name = "Col. var."
    
    if (!is.null(svg.path)) {
    # Save graph as svg
        svg(paste(svg.path,"/","dpca.svg",sep=""))
        plot(c(Cc[, 1], Cl[, 1]), c(Cc[, 2], Cl[, 2]), xlab = "Axis 1", ylab = "Axis 2", type="n", 
            main=main1, ...)
        abline(h = 0, v = 0) 

        text(Cc[, 1], Cc[, 2], labels = Label2, col="Blue")
        text(Cl[, 1], Cl[, 2], labels = Label1, vfont = c("serif", "italic"), cex=0.8, col="Red")
        legend(lpos, c(var1.name, var2.name), fill=c("Red", "Blue"), text.font = c(3,1))
        dev.off()
        
        svg(paste(svg.path,"/","biplot.svg",sep=""))
        plot(c(DI.c[, 1], DI.l[, 1]), c(DI.c[, 2], DI.l[, 2]), xlab = "Axis 1", ylab = "Axis 2", type="n", 
         main=main2, ...)
        abline(h = 0, v = 0) 
        xxc = DI.c[, 1]
        yyc = DI.c[, 2]
        text(xxc, yyc, labels = Label2, col="Blue")

        xxl = DI.l[, 1]
        yyl = DI.l[, 2]
        text(xxl, yyl, labels = Label1, vfont = c("serif", "italic"), cex=0.8, col="Red")
        legend(lpos, c(var1.name, var2.name), fill=c("Red", "Blue"), text.font = c(3,1))
        dev.off()}

    plot.new()

    plot(c(Cc[, 1], Cl[, 1]), c(Cc[, 2], Cl[, 2]), xlab = "Axis 1", ylab = "Axis 2", type="n", 
         main=main1, ...)
    abline(h = 0, v = 0) 

    text(Cc[, 1], Cc[, 2], labels = Label2, col="Blue")
    text(Cl[, 1], Cl[, 2], labels = Label1, vfont = c("serif", "italic"), cex=0.8, col="Red")
    legend(lpos, c(var1.name, var2.name), fill=c("Red", "Blue"), text.font = c(3,1))
    
        
    # principal components

    # Representation by Biplot

    plot.new()
    plot(c(DI.c[, 1], DI.l[, 1]), c(DI.c[, 2], DI.l[, 2]), xlab = "Axis 1", ylab = "Axis 2", type="n", 
         main=main2, ...)
    # plot(DI.c[, 1], DI.c[, 2], xlab = "Axe1", ylab = "Axe2", type='p')
    # plot(DI.l[, 1], DI.l[, 2], xlab = "Axe1", ylab = "Axe2", type='s', add=T)
    abline(h = 0, v = 0) 
    xxc = DI.c[, 1]
    yyc = DI.c[, 2]
    text(xxc, yyc, labels = Label2, col="Blue")

    xxl = DI.l[, 1]
    yyl = DI.l[, 2]
    text(xxl, yyl, labels = Label1, vfont = c("serif", "italic"), cex=0.8, col="Red")
    legend(lpos, c(var1.name, var2.name), fill=c("Red", "Blue"), text.font = c(3,1))
    
    res = NULL
    res$pc.rows = Cl
    res$pc.cols = Cc
    res$chisq = chisq.test(ContTab)
    
    return(res)
}