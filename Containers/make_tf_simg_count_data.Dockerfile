# Dockerfile for use with bnp-mrf

#Download base image ubuntu 18.04
FROM ubuntu:18.04
LABEL maintainer="Jean-Baptiste Durand <jean-baptiste.durand@inria.fr>"
LABEL version="1.0"
LABEL vcs-url="gitlab.inria.fr:statify_public/anzj-crash.git"
LABEL vcs-ref="a8720b7005ddb067eef326b834150ea6c5125afe"
LABEL docker.build.cmd="docker build -f Containers/make_tf_simg_count_data.Dockerfile -t jbdurand/anzj-crash:1.1 ."
LABEL docker.cmd="docker run --rm -it -p 8888:8888 jbdurand/anzj-crash:1.1"
LABEL docker.cmd.variant="docker run -v my_library_path:/scratch:rw --user root --rm -it -p 8888:8888 jbdurand/anzj-crash:1.1"
LABEL usage="jupyter notebook --ip 0.0.0.0 --no-browser --allow-root"

# usual other labels
# docker.cmd: How to run a container based on the image under the Docker runtime.
# docker.cmd.test: How to run the bundled test-suite for the image under the Docker runtime.
# docker.params

# Find your id with "id -u my_login"

# Environment variables
# ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

ENV LD_LIBRARY_PATH="/usr/local/cuda/lib64/:/usr/local/nvidia/lib:/usr/local/nvidia/lib64"

ENV NVIDIA_VISIBLE_DEVICES="all"
ENV NVIDIA_DRIVER_CAPABILITIES="compute,utility"
ENV NVIDIA_REQUIRE_CUDA="cuda>=10.0"

ENV CUDA_VERSION="10.0.130"
ENV CUDA_PKG_VERSION="10-0=$CUDA_VERSION-1"
ENV CUDNN_VERSION="8.0.5"
# Update the image & install some tools
RUN apt update && apt install -y gedit 

RUN apt install -y --no-install-recommends \
    ca-certificates apt-transport-https gnupg curl dirmngr vim cmake
RUN apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
RUN echo "deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/cuda.list
RUN echo "deb https://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/nvidia-ml.list

RUN apt update --allow-insecure-repositories


RUN apt install -y software-properties-common && \
    add-apt-repository ppa:deadsnakes/ppa && \
    apt update && \
    apt install -y python3.7-minimal

RUN apt install -y --no-install-recommends \
    cuda-nvrtc-$CUDA_PKG_VERSION \
    cuda-nvgraph-$CUDA_PKG_VERSION \
    cuda-cusolver-$CUDA_PKG_VERSION \
    cuda-cublas-$CUDA_PKG_VERSION \
    cuda-cublas-dev-$CUDA_PKG_VERSION \
    cuda-cufft-$CUDA_PKG_VERSION \
    cuda-curand-$CUDA_PKG_VERSION \
    cuda-cusparse-$CUDA_PKG_VERSION \
    cuda-npp-$CUDA_PKG_VERSION \
    cuda-cudart-$CUDA_PKG_VERSION 

RUN ln -s cuda-10.0 /usr/local/cuda

ENV CUDNN_VERSION="8.0.5.39"
 
RUN apt install -y --no-install-recommends --allow-unauthenticated \
    libcudnn8="$CUDNN_VERSION-1+cuda11.1" \
    libcudnn8-dev="$CUDNN_VERSION-1+cuda11.1" 

RUN echo "/usr/local/nvidia/lib" >> /etc/ld.so.conf.d/nvidia.conf
RUN echo "/usr/local/nvidia/lib64" >> /etc/ld.so.conf.d/nvidia.conf

RUN apt update && \
    apt install -y python3-pip && \
    apt install -y  libx11-dev \
                    libopenblas-dev \
                    liblapack-dev \                    
                    pkg-config \
                    libpython3.7-dev
                    
RUN python3.7 -m pip install --upgrade pip

RUN python3.7 -m pip install py==1.8.1 && \
    python3.7 -m pip install urllib3==1.25.8  && \
    python3.7 -m pip install pylint==2.5.2  && \
    python3.7 -m pip install wordcloud==1.7.0 && \
    python3.7 -m pip install tornado==6.0.4 && \
    python3.7 -m pip install theano==1.0.4 && \
    python3.7 -m pip install cython==0.29.17 && \
    python3.7 -m pip install dlib==19.21.1 && \
    python3.7 -m pip install h5py==2.8.0 && \
    python3.7 -m pip install html5lib==1.0.1 && \
    python3.7 -m pip install jupyter==1.0.0 && \
    python3.7 -m pip install joblib && \
    python3.7 -m pip install llvmlite==0.32.1 && \
    python3.7 -m pip install nltk==3.4.5 && \
    python3.7 -m pip install notebook==6.0.3 && \
    python3.7 -m pip install matplotlib==3.1.3 && \
    python3.7 -m pip install numba==0.49.1 && \
    python3.7 -m pip install numpy==1.19.5 && \
    python3.7 -m pip install opencv-python==4.4.0.42 && \
    python3.7 -m pip install pandas==1.0.3 && \
    python3.7 -m pip install pillow && \
    python3.7 -m pip install scikit-image==0.16.2 && \
    python3.7 -m pip install scikit-learn==0.22.1 && \
    python3.7 -m pip install scipy==1.4.1 && \
    python3.7 -m pip install seaborn==0.10.1 && \
    python3.7 -m pip install tensorflow-gpu==2.4.0 && \
    python3.7 -m pip install keras==2.1.5 && \
    python3.7 -m pip install xlrd==1.2.0 && \
    python3.7 -m pip install pymc3==3.10.0

# Install R packages
RUN apt install -y apt-transport-https && \
    apt update

ENV R_REPOS="https://cloud.r-project.org"

RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 51716619E084DAB9

RUN add-apt-repository "deb $R_REPOS/bin/linux/ubuntu bionic-cran35/"

RUN apt update

# To find R versions
# apt-cache policy r-base

ENV R_VERSION="3.6.3-1bionic" R_CONTRIBS="https://cloud.r-project.org" 

ENV DEBIAN_FRONTEND=noninteractive

RUN ln -fs /usr/share/zoneinfo/Europe/Paris /etc/localtime && \
    apt-get install -y tzdata && \
    dpkg-reconfigure --frontend noninteractive tzdata

RUN apt install -y r-base=$R_VERSION && \ 
    apt install -y libudunits2-dev && \
    apt install -y libgdal-dev

RUN mkdir /root/r_analysis &&\
    cd /root/r_analysis &&\
    echo 'install.packages("INLA", repos=c("'$R_CONTRIBS'", INLA="https://inla.r-inla-download.org/R/testing"), dep=TRUE)' >> r_install.txt &&\
    echo 'install.packages("diseasemapping", repos="'$R_CONTRIBS'")' >> r_install.txt &&\
    echo 'install.packages("sp", repos="'$R_CONTRIBS'")' >> r_install.txt &&\
    echo 'install.packages("spdep", repos="'$R_CONTRIBS'")' >> r_install.txt &&\
    echo 'install.packages("geostatsp", repos="'$R_CONTRIBS'")' >> r_install.txt &&\
    echo 'install.packages("mapmisc", repos="'$R_CONTRIBS'")' >> r_install.txt &&\
    Rscript r_install.txt &&\
    rm -Rf /root/r_analysis

    
# Install rpy2

RUN python3.7 -m pip install rpy2==3.4.2
RUN python3.7 -m pip install openpyxl==3.0.9

# Set arguments
ARG user=stat
ARG home=/home/stat/
ARG devlp=/home/stat/devlp/
# Create the user and its directory
RUN mkdir -p $home &&\
    useradd $user --home-dir $home && \
    chown -R $user: $home && \
    mkdir -p $devlp/bnp_mrf && \
    cd $devlp

RUN mkdir /scratch    
# Variant to build from sources
# git clone https://gitlab.inria.fr:bnp/bnp-mrf.git

# To be run from bnp_mrf

# otherwise: 
# COPY --chown=stat ./notebooks/ /home/stat/devlp/bnp_mrf/notebooks
# Does not work (docker version?)

COPY ./notebooks/ /home/stat/devlp/bnp_mrf/notebooks
COPY ./data  /home/stat/devlp/bnp_mrf/data
COPY ./lib  /home/stat/devlp/bnp_mrf/lib
COPY ./main  /home/stat/devlp/bnp_mrf/main
COPY ./R-functions /home/stat/devlp/bnp_mrf/R-functions
COPY ./README.txt /home/stat/devlp/bnp_mrf/README.txt

RUN chown -R stat /home/stat/devlp/bnp_mrf/

RUN python3.7 -m pip install simplegeneric
RUN apt -y autoremove

# remove cache
RUN rm -rf /var/lib/apt/lists/*

# Switch to the new user
USER $user
# USER root

# Change working directory
WORKDIR /home/stat/devlp/bnp_mrf/notebooks/

# Set the default entry point & arguments
# ENTRYPOINT ["jupyter", "notebook", "--no-browser"]
# CMD ["--port=8888", "--ip='*'",  "--NotebookApp.token=''"]

# Once in the container
# jupyter notebook --ip 0.0.0.0 --no-browser --allow-root
# Will not start a browser but the host can use its browser with
# returned URL

