BNP-MRF models v1.0: Application to crash data

This file contains instructions on how to work with the source code for performing risk mapping based on the BNP-MRF mixture models. 

1. Run the code with Docker
==================================
docker run --rm -it -p 8888:8888 registry.gitlab.inria.fr/statify_public/anzj-crash/anzj-crash:1.1
jupyter notebook --ip 0.0.0.0 --no-browser --allow-root

# Besides the command above using an Inria Gitlab docker image, you may rebuild the docker environment from the sources:
# git clone https://gitlab.inria.fr/statify_public/anzj-crash.git
# docker build -f Containers/make_tf_simg_count_data.Dockerfile -t jbdurand/anzj-crash:1.1 
# docker run --rm -it -p 8888:8888 jbdurand/anzj-crash:1.1 
# jupyter notebook --ip 0.0.0.0 --no-browser --allow-root


As an alternative, you may build a conda environment with 
conda env create -f Containers/conda_bnp_mrf.yml
activate the environment with
conda activate conda_bnp_mrf
and run jupyter:
jupyter notebook

2. Structure of package
==================================
apps: external libraries dedicated to specific applications
Containers: definition files for Docker and Singularity images
examples: illustration of model validation with external libraries
lib: the python libraries (.py scripts) for model estimation  
R-functions: R functions for data visualization
data: data sets        
main: main python scripts, interface to the bnp-mrf library  
notebooks: available notebooks  
test: test files for main and lib .py scripts

3. Structure of the source code
==================================
main/disease_map_main.py is the main script for analysing graphical models with continuous or count data
main/img_seg_main.py is the main script for image segmentation with Gaussian models
lib/VBEM.py: main class representing the BNP-MRF model and VBEM algorithm
lib/pitmanyor.py: module dedicated to implementation of Pitman-Yor processes. Dirichlet processes are recovered by setting sigma to 0.   
lib/potts_model.py: specific algorithm for estimation and approximation regarding MRF parameter 
lib/distribution.py: A collection of emission distributions. Parent class of gaussian, poisson, etc.
lib/gaussian.py: Gaussian emission distributions
lib/poisson.py: Poisson (regression) emission distributions
lib/binomial.py: binomial emission distributions           
lib/preprocessing.py: data preprocessing and other utilities
lib/processing_utils.py: input file processing
lib/vbem_utils_numba.py: module dedicated to VBEM steps relying on numba (acceleration by automatic parallelization)
lib/evaluation_utils.py: basic functions for performance evaluation PRI
lib/bnp.py: sampling with Pitma-Yor / Dirichlet processes

