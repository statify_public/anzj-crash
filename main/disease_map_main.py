import numpy as np
import matplotlib.pyplot as plt
import os
import sys
import argparse
import time
import pickle

lib_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), "..")) \
            + "/lib"
sys.path.append(lib_dir)
from VBEM import VBEM
from preprocessing import get_data_and_neighbors, count_clusters, \
    check_neighbors, save_adjacency_matrix

def list_float(string):
    """
    Conversion from string to list of floats
    """
    error = False
    try:
        l = eval(string)
    except (SyntaxError):
        error = True
    if not(error) and type(l) != list:
        error = True
    if error:
        msg = "%r is not a valid list" % string
        raise argparse.ArgumentTypeError(msg)
    for f in l:
        try:
            v = float(f)
        except (ValueError):
            error = True
        else:
            if v < 0. or  v > 1.:
                error = True
    if error:
            msg = "%r is not a valid list of floats in (0,1)" % string
            raise argparse.ArgumentTypeError(msg)
    return (l)

start_time = time.time()

default_maxi = 9998
default_seed = 0
default_nbinit  = 100
default_threshold = 1.0e-3

parser = argparse.ArgumentParser(description="VBEM for BNP-MRF models v2.0: \
                                            Application to disease mapping")
parser.add_argument("--data", type=str, required=True,
                    help="path of the data")
parser.add_argument("--neighbors", type=str, required=True,
                    help="path of the neighborhood")
parser.add_argument("--output_folder", type=str, required=True,
                    help="path of the output folder for segmentation results")
parser.add_argument("--k_trunc", type=int, required=True,
                    help="truncation of stick-breaking")
parser.add_argument("--model", type=str, required=True,
                    help="dp, py, dp-mrf or py-mrf")
parser.add_argument("--maxi", type=int, default=default_maxi,
                    help="maximum number of iterations")
parser.add_argument("--threshold", type=float, default=default_threshold,
                    help="the convergence threshold")
parser.add_argument("--emission_distribution", type=str, default="poisson",
                    help="the name of the emission distribution under consideration, poisson or binomial, default: poisson")
parser.add_argument("--seed", type=int, default=default_seed,
                    help="value of seed to be used in k-means initialization")
parser.add_argument("--nbinit", type=int, default=default_nbinit,
                    help="number of runs of k-means initialization")
parser.add_argument("--init_method", type=str, default="k_means_only",
                    help="method used in initialization (free_energy, k_means_only or file)")
parser.add_argument("--init_file", type=str, default="",
                    help="name of initialization file containing labels")
parser.add_argument("--alpha_estim", type=str, default="q_alpha",
                    help="method for alpha estimation (q_alpha or free_energy)")
parser.add_argument("--beta_estim", type=str, default="M_beta",
                    help="method for beta estimation (M_beta, MC_beta or free_energy)")
parser.add_argument("--return_model", type=str, default="final",
                    help="model returned (final or best_free_energy)")
parser.add_argument("--mc_iters", type=int, default=0,
                    help="number of Monte-Carlo iterations in beta_estim = 'MC_beta'")
args = parser.parse_args()

# Required inputs
data_path = args.data
neighbors_path = args.neighbors
output_folder = args.output_folder
k_trunc = args.k_trunc
model = args.model
# Optional inputs
emission_dist = args.emission_distribution
threshold = args.threshold
maxi = args.maxi
seed = args.seed
nbinit = args.nbinit
init_method = args.init_method
init_file = args.init_file
alpha_estim = args.alpha_estim
beta_estim = args.beta_estim
returnv = args.return_model
mc_iters = args.mc_iters
if mc_iters <= 0:
    mc_iters = None

# Check the given emission distribution
if not(emission_dist in ["gaussian", "poisson", "binomial"]):
    print("Warning: Please check the emission distribution name.")
    raise SystemExit

if emission_dist == "gaussian":
    dtype = np.float64
else:
    dtype = np.int64

# Check initialization method
if not(init_method in ["free_energy", "k_means_only", "file"]):
    print("Warning: Please check initialization method.")
    raise SystemExit
elif init_method=="file":
    if seed != default_seed:
        print("Warning: argument seed will not be used since init_method \
              is 'file'")
    if nbinit != default_nbinit:
        print("Warning: argument nbinit will not be used since init_method \
              is 'file'")
              
# Preprocessing the input data NxD with N the # of obs and D the # of features 
data, neighborhood = get_data_and_neighbors(data_path, neighbors_path, dtype)

check_neighborhood = False # validity of neighbors

if check_neighborhood:
    nb = check_neighbors(neighborhood)
    save_adjacency_matrix(data_path, nb)
    
print("ktrunc=" + str(k_trunc), 
        "model=" + str(model), 
        "data_dim=" + str(data.shape))            

    
# Run the VBEM algorithm
vbem = VBEM(emission_dist, model, k_trunc, nbinit, init_method, init_file)
c_est, q_z, m_a, m_s, corr_as, beta, delta_qz, elbo, best_iter, max_deltas = \
    vbem.run(data, neighborhood, threshold, maxi, seed, nbinit, 
             alpha_estim, beta_estim, returnv, mc_iters)

# Clusters, state probabilities, sequence of mean alphas and sigmas, alpha corrs,
# beta, qz norm diff, sequence of elbo, iteration with max elbo, 
# value of stopping criterion

# Risks computation
risks = vbem.py.parameter
hyper = vbem.py.prior
hypers = {} # sorted

if hasattr(risks, 'shape') and len(risks.shape) == 1:
    # sort parameters by increasing order
    perm = np.argsort(risks)
    inv_perm = np.array(perm)
    for i in range(risks.shape[0]):
        inv_perm[perm[i]] = i
    risks.sort()
    # risks[inv_perm[i]] has rank i when risks are sorted
    # inv_perm[i] is the new label of i
    P = np.zeros((k_trunc, k_trunc))
    for i in range(len(c_est)):
        c_est[i] = inv_perm[c_est[i]]
    for i in range(k_trunc):
        P[i, perm[i]] = 1.
    q_z = np.matmul(q_z, P.T)
    for k, a in hyper.items():
        v = np.array(a)
        for i in range(k_trunc):
            v[inv_perm[i]] = a[i]
        hypers[k] = v
    
# Gaussian case: replace risks by 1st dimension of means
if (emission_dist == "gaussian"):
    print(risks)
    risks = risks["mean"]
    k_est = risks.shape[0]
    risks = risks[0:(k_est+1),0]

# Count clusters
clusters, H = count_clusters(c_est, q_z, k_trunc, data.shape[0])
nb_clusters = len(np.unique(clusters))

if m_a.shape[0] > 0:
    # Print the results
    print("---results----")
    print("Final nk:", q_z.sum(axis=0))
    print("c_est= {} ".format(c_est.shape))
    print("nb_iter= {}  nb_clusters= {} {}".format(m_a.shape, nb_clusters, np.unique(c_est)))
    print("risk= {}".format(risks))
    print("alpha= {} s1={} s2={} sigma={}".format(m_a[best_iter], vbem.process.alpha[0], \
                              vbem.process.alpha[1], m_s[best_iter]))
    print("corr_as=" + str(corr_as))
    print("beta=" + str(beta[best_iter]))
    print("delta_qz=" + str(delta_qz[best_iter]))
    print("max_delta=" + str(max_deltas))

    # Plots 
    fig_handle = plt.figure()
    
    fig, axs = plt.subplots(4, 1, constrained_layout=True)
    axs[0].plot(np.array(range(len(beta))), beta, 'o')
    axs[0].set_title('Beta')
    axs[1].plot(np.array(range(len(m_a))), m_a, 'o')
    axs[1].set_title('Alpha')
    axs[2].plot(np.array(range(len(delta_qz[1:]))), delta_qz[1:], 'o')
    axs[2].set_title('delta_q_z')
    axs[3].plot(np.array(range(len(elbo)-1)), elbo[1:], 'o')
    axs[3].set_title('elbo')

    with open(os.path.join(output_folder, 'fig.pickle'), 'wb') as f: 
        pickle.dump(fig_handle, f) 
    
    plt.savefig(os.path.join(output_folder, 'fig.svg'), format='svg')
    plt.savefig(os.path.join(output_folder, 'fig.png'), format='png')

    # plt.show()

# save information for graphical representation
data_name = os.path.splitext(os.path.basename(data_path))[0]

output_cluster = os.path.join(output_folder, \
		"labels_{}_{}_ktrunc-{}.csv".format(data_name, \
		model, k_trunc))
np.savetxt(output_cluster, c_est, delimiter=",", header="")

output_risks = os.path.join(output_folder, \
		"risks_est_{}_{}_ktrunc-{}.csv".format(data_name, \
		model, k_trunc))
np.savetxt(output_risks, risks, delimiter=",", header="")
"""
# save the posterior distribution of the risks

output_ak = os.path.join(output_folder, \
		"ak_{}_{}_ktrunc-{}.csv".format(data_name, \
		model, k_trunc))
np.savetxt(output_ak, vbem.py.prior["a"], delimiter=",", header="")

output_bk = os.path.join(output_folder, \
		"bk_{}_{}_ktrunc-{}.csv".format(data_name, \
		model, k_trunc))
np.savetxt(output_bk, vbem.py.prior["b"], delimiter=",", header="")
"""

output_q_z = os.path.join(output_folder, \
		"q_z_{}_{}_ktrunc-{}.csv".format(data_name, \
           model, k_trunc))
np.savetxt(output_q_z, q_z, delimiter=",", header="")

output_H = os.path.join(output_folder, \
		"entropy_{}_{}_ktrunc-{}.csv".format(data_name, \
           model, k_trunc))
np.savetxt(output_H, H, delimiter=",", header="")

output_hyper = os.path.join(output_folder, \
		"hyper_{}_{}_ktrunc-{}.csv".format(data_name, \
           model, k_trunc))

h = "" # header
ncol = len(hypers)
hypers_np = np.zeros((k_trunc, ncol), dtype=np.float64)
col = 0
for (k, v) in hypers.items():
    hypers_np[:,col] = v
    col += 1
    h += str(k) + ","
h = h[0:-1]
np.savetxt(output_hyper, hypers_np, delimiter=",", header=h)

print("--- %s seconds ---" % (time.time() - start_time))

