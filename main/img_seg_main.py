import numpy as np
import os
import sys
import argparse
import time
import pickle
start_time = time.time()

lib_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), "..")) \
            + "/lib"

sys.path.append(lib_dir)


from VBEM import VBEM
from processing_utils import im2pixel
from processing_utils import neighbor_list
from processing_utils import im2superpixel
from processing_utils import find_neighbors
from processing_utils import image_restored
from evaluation_utils import gt2array
from evaluation_utils import compute_pri


parser = argparse.ArgumentParser(description="VBEM for BNP-MRF models v2.0: \
                                            Application to image segmentation")
parser.add_argument("--input_img", type=str, required=True,
                    help="path of the input image for segmentation")
parser.add_argument("--img_type", type=str, required=True,
                    help="image type, grayscale or color")
parser.add_argument("--feature_type", type=str, required=True,
                    help="feature type, px or spx")
parser.add_argument("--output_folder", type=str, required=True,
                    help="path of the output folder for segmentation results")
parser.add_argument("--k_trunc", type=int, required=True,
                    help="truncation of stick-breaking")
parser.add_argument("--model", type=str, required=True,
                    help="dp, py, dp-mrf or py-mrf")
parser.add_argument("--texture", type=str, default="yes",
                    help="use or not use MR8 filter bank for texture \
                        feature extraction")
parser.add_argument("--nb_neighbors", type=int, default=8,
                    help="number of neighbors for each site (only for \
                        superpixels)")
parser.add_argument("--maxi", type=int, default=9998,
                    help="maximum number of iterations")
parser.add_argument("--threshold", type=float, default=1.0e-3,
                    help="the convergence threshold")
parser.add_argument("--ms_bandwidth", type=float, default=0.0,
                    help="set the bandwidth of MeanShift for region merging")
parser.add_argument("--nb_sps", type=int, default=1000, 
                    help="approximate number of superpixels given by SLIC")
parser.add_argument("--nb_init", type=int, default=1, 
                    help="number of runs for initialization procedure")
parser.add_argument("--input_gt", type=str, 
                    help="path of the ground truth if available")
# for regression tests reproducibility
parser.add_argument("--exec_mode", type=str, default="run",
                    help="run or test")
# Set seed also for reproducibility
parser.add_argument("--seed", type=int, default=0,
                    help="value of seed to be used in k-means initialization")
parser.add_argument("--nbinit", type=int, default=100,
                    help="number of runs of k-means initialization")
parser.add_argument("--init_method", type=str, default="k_means_only",
                    help="method used in initialization (free_energy or k_means_only)")
parser.add_argument("--alpha_estim", type=str, default="q_alpha",
                    help="method for alpha estimation (q_alpha or free_energy)")
parser.add_argument("--beta_estim", type=str, default="M_beta",
                    help="method for beta estimation (M_beta or free_energy)")
parser.add_argument("--return_model", type=str, default="final",
                    help="model returned (final or best_free_energy)")
parser.add_argument("--mc_iters", type=int, default=0,
                    help="number of Monte-Carlo iterations in beta_estim = 'MC_beta'")
args = parser.parse_args()

# Required inputs
input_img = args.input_img
img_type = args.img_type
feature_type = args.feature_type
output_folder = args.output_folder
k_trunc = args.k_trunc
model = args.model
# Optional inputs
texture = args.texture
input_gt = args.input_gt
threshold = args.threshold
ms_bandwidth = args.ms_bandwidth
maxi = args.maxi
nb_neighbors = args.nb_neighbors
seed = args.seed
nbinit = args.nbinit
init_method = args.init_method
alpha_estim = args.alpha_estim
beta_estim = args.beta_estim
returnv = args.return_model
mc_iters = args.mc_iters
if mc_iters <= 0:
    mc_iters = None

# for regression tests reproducibility
if args.exec_mode == "test":
	np.random.seed(222)

# Preprocessing the input data NxD with N the # of obs and D the # of features 
data = np.array([])
presegs = np.array([])
neighborhood = np.array([])
'''
from processing_utils import preprocess_data_rats
data, neighborhood = \
    preprocess_data_rats("Rats_Sain_-_Brain_-_Lemasson_2016_meta.csv")
'''

if feature_type == "px":
    # Read input data
    data, presegs, centers = im2pixel(input_img, img_type, texture)
    # Find 8 neighbor indexes for each pixel
    neighborhood = neighbor_list(input_img)
elif feature_type == "spx":
    # Approximate number of superpixels
    nb_sps = args.nb_sps
    # Read input data
    data, presegs, centers = im2superpixel(input_img, img_type, 
                                                texture, nb_sps)
    # Find 8 neighbor indexes for each superpixel
    neighborhood = find_neighbors(centers, nb_neighbors)
else:
    print("Warning: please check the feature type.")
    raise SystemExit

print("ktrunc=" + str(k_trunc), "model=" + str(model), 
      "img_type=" + str(img_type), "feature_type=" + str(feature_type), 
      "texture=" + str(texture), "data_dim=" + str(data.shape))

# Run the VBEM algorithm
vbem = VBEM("gaussian", model, k_trunc, nbinit, init_method)
c_est, q_z, m_a, m_s, corr_as, beta, delta_qz, elbo, best_iter, max_deltas = \
    vbem.run(data, neighborhood, threshold, maxi, seed, nbinit, 
             alpha_estim, beta_estim, returnv)

# Region merging by MeanShift
(nb_data, dim_data) = np.shape(data)
m_niw = vbem.py.prior["m"]
if ms_bandwidth > 0.0:
    from sklearn.cluster import MeanShift
    ms = MeanShift(bandwidth=ms_bandwidth, bin_seeding=True).fit(m_niw)
    new_labels = ms.labels_
    new_labels_ids = np.unique(ms.labels_)
    nb_cls = len(new_labels_ids)
    new_means = np.zeros((nb_cls, dim_data), dtype=np.float64)
    idx = 0
    for i in new_labels_ids:
        labels_to_replace = np.nonzero(new_labels == i)[0]
        for c in labels_to_replace:
            indexes = np.nonzero(c_est == c)[0]
            c_est[indexes] = i
        aux = m_niw[labels_to_replace]
        new_means[idx, :] = np.mean(aux, axis=0)
        idx = idx + 1
    m_niw = new_means
    
    c_est = c_est.reshape((nb_data, 1))


means, labels = image_restored(input_img, img_type, feature_type, 
                            texture, presegs, m_niw, c_est)

# Determine the number of clusters
nb_clusters = len(np.unique(labels))
nk = q_z.sum(axis=0) + 10 * np.finfo(q_z.dtype).eps
nb_clusters = np.count_nonzero(np.int64(nk))

# Print the results
print("Final nk:", q_z.sum(axis=0))
print("alpha=" + str(m_a[best_iter]))
print("sigma=" + str(m_s[best_iter]))
print("corr_as=" + str(corr_as))
print("beta=" + str(beta[best_iter]))
print("nb_clusters=" + str(nb_clusters))
print("delta_qz=" + str(delta_qz[best_iter]))
print("max_delta=" + str(max_deltas))

# Save the results
img_name = os.path.splitext(os.path.basename(input_img))[0]
output_res = output_folder + "/" + img_name + "_" + model \
            + "_ktrunc-" + str(k_trunc)

pri = 0.0
if input_gt:
    gt, nb_segs, height, width = gt2array(input_gt)
    pri = compute_pri(gt, labels, nb_segs, height, width)
    print("pri=" + str(pri))
else:
    # Plot segmentation
    import matplotlib.pyplot as plt
    
    fig_handle = plt.figure()
    with open(os.path.join(output_folder, 'fig.pickle'), 'wb') as f: 
        pickle.dump(fig_handle, f) 
    plt.imshow(presegs)
    plt.savefig(os.path.join(output_folder, 'fig.png'), format='png')    
    
    # plt.show()

np.savez_compressed(output_res, means=means, 
                                labels=labels, 
                                presegments=presegs, 
                                m_a=m_a, 
                                m_s=m_s, 
                                corr_as=np.array([corr_as]), 
                                beta=beta, 
                                delta_qz=delta_qz, 
                                pri=np.array([pri]), 
                                nb_clusters=np.array([nb_clusters]))

print("--- %s seconds ---" % (time.time() - start_time))

