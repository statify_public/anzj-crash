import numpy as np
import scipy
import scipy.optimize as optimize 
from vbem_utils_numba import gradient_beta, gradient_beta_mc_generic, \
    gradient_beta_concave, gradient_beta_mc_concave_generic, \
    part1_gradient_beta, part2_gradient_beta,  \
    compute_z_tau_entropy,  compute_tau_tilde, compute_log_pi_tilde

def compact_search_index(search, part):
    """
    Delete successive False in search and associated indices in part
    """
    s = np.array([], np.bool)
    p = np.array([part[0]])
    stop = False
    i = 0
    pos = 0
    while not(stop):        
        posn = find_maximal_sequence(False, pos, search)
        if posn > pos:
            # Found a maximal sequence of true: compact search and part
            if posn < len(part):
                s = np.insert(s, i, False)
                p = np.insert(p, i+1, part[posn])
            if posn >= len(part):
                # False up to the end
                s = np.insert(s, i, False)
                p = np.insert(p, i+1, part[len(part)-1])
            pos = posn
        else:
            # Copy next value
            if pos+1 < len(search):
                s = np.insert(s, i, search[pos+1])
                p = np.insert(p, i+1, part[pos+1])
            pos += 1
        i += 1
        stop = (pos >= len(search))
            
    return(s, p)

def init_update_beta_brent(gradient_beta, q_z, gamma_sb, nb_data, k_trunc, 
                           neighborhood, nb_contrib, maxiter=300):
    """
    Initialize optimize.brentq in cancelling beta gradient    
    """
    min_beta = -0.5
    max_beta = 0.5

    nb_iter = 0
    # Check that gradient_s_dp change signs between min_s1 and max_s1
    iterate =  gradient_beta(min_beta, q_z, gamma_sb, nb_data, k_trunc, 
                             neighborhood, nb_contrib) * \
        gradient_beta(max_beta, q_z, gamma_sb, nb_data, k_trunc, 
                      neighborhood, nb_contrib)
    while (nb_iter < maxiter) and iterate > 0:
        min_beta = min_beta * 2
        max_beta = max_beta * 2
        iterate =  gradient_beta(min_beta, q_z, gamma_sb, nb_data, 
                                 k_trunc, neighborhood, nb_contrib) * \
            gradient_beta(max_beta, q_z, gamma_sb, nb_data, k_trunc, 
                          neighborhood, nb_contrib)
        nb_iter += 1
    
    if iterate > 0:
        # Initialization failed
        import sys
        min_beta = -sys.float_info.max
        max_beta = sys.float_info.max
        
    return min_beta, max_beta

def find_maximal_sequence(v, pos, l):
    """
    Find maximal sequence of v from pos in iterable l
    Return index of last found value + 1
    """
    p = pos
    while p < len(l):
        if l[p] != v:
            return(p)
        p += 1
    return(p)

def find_multiple_roots(f, a, b, args=(), opt=optimize.brentq, depth=10, xtol=2e-12, 
                        rtol=8.881784197001252e-16, maxiter=100, full_output=False):
    """
    Find multiple roots of a function using some solver
    
    :Returns:
        (flag, roots) with flag = 0 if nb_roots <= max_nb_roots, 
            flag = 1 if nb_roots = 0, flag = 2 if nb_roots > max_nb_roots
    """
    max_search_it = 10000 # maximal number of iterations
    max_nb_roots = 1000 # Stop searching roots if the number of roots is higher
    max_nb_candidates = 2000 # Stop searching roots if the number of root
    # candidates is higher
    
    flag = 0
    # Stopping condition: no root found between any elements of part (segment)
    roots = np.array([])
    # Boolean array: is a root to be searched in each segment?
    found_all = False
    # Initialize search: find values with different f signs 
    my_f = lambda x: f(x, *args)
    try:
        sub = np.arange(a, b, (b-a)/(2.**depth))
    except ValueError: # as e:
        # b-a may exceed largest float
        a = -4.
        b = 8.
        sub = np.arange(a, b, (b-a)/(2.**depth))
    sub = np.insert(sub, len(sub), b)
    vals = np.array(list(map(my_f, sub)))
    prods = vals[0:-1] * vals[1:len(vals)]
    # Search_roots[r] true iff search root in (part[r], part[r+1])
    search_roots = np.array([v < 0 for v in prods])
    # subdivision bounding multiple roots
    part = np.copy(sub)
    
    # Check if by any chance f(val) == 0
    roots = np.insert(roots, 0, sub[np.where(abs(vals) < xtol)[0]])
    # Search for other roots even if roots is not empty.
    
    it = 1

    if np.all(prods > 0):
        flag = 1
        return flag, roots
    
    while not(found_all or (it > max_search_it)):
        """
        if debug:
            msg = "search_roots: " + str(search_roots)
            msg += "; part: " + str(part)
            print(msg)
        """
        if search_roots.sum() > max_nb_candidates:
            if len(roots) > max_nb_roots:
                msg = "Found too many roots to beta gradient: " 
                msg += str(len(roots)) + "; aborting research"
                flag = 2
            else:
                msg = "Found too many candidate roots to beta gradient: " 
                msg += str(search_roots.sum()) + "; aborting research"
                if len(roots) == 0:
                    flag = 1
                else:
                    flag = 0
            print(msg)
            # q_z, gamma_sb, nb_data, k_trunc, nb_contrib = args
            return flag, roots
        if len(search_roots) > 2 * max_nb_candidates:
                search_roots, part =  compact_search_index(search_roots, part)
        for r in range(len(search_roots)):
            # if segment marked to be searched
            if search_roots[r]:
                try:
                    x, res = opt(f, part[r], part[r+1], args=args, 
                                 maxiter=maxiter, xtol=xtol, 
                                 rtol=rtol, full_output=True)
                except ValueError:
                    search_roots[r] = False
                else:
                    if res.converged:
                        # found root
                        # Insert new root
                        i = np.searchsorted(roots, x)
                        roots = np.insert(roots, i, x)
                        # It is assumed that no new root will be found 
                        # in (x-2*xtol, x+2*xtol)
                        part[r+1] = x - 2*xtol
                        # Insert  (x-2*xtol, x+2*xtol) and mark as not searchable
                        part = np.insert(part, r+2, x + 2*xtol)
                        search_roots = np.insert(search_roots, r+1, False)
                        # Insert  (x+2*xtol, old_part[r+1]) and mark as searchable
                        part = np.insert(part, r+3, part[r+2])
                        search_roots = np.insert(search_roots, r+2, True)
                    else:
                        search_roots[r] = False
        found_all = np.all(np.invert(search_roots))
        it += 1
        
    if len(roots) == 0:
        flag = 1
    else:
        flag = 0
        
    return flag, roots
    

def update_beta_z(q_z, gamma_sb, model, sampling_model, nb_data, 
                  k_trunc, beta_z_0, neighborhood, nb_contrib, iters, mc_iters, 
                  taus=None, beta_estim="M_beta"):

    if beta_estim == "free_energy":
        beta_z = update_beta_z_elbo(q_z, gamma_sb, model, sampling_model, 
                                    nb_data, k_trunc, beta_z_0,
                                    neighborhood, nb_contrib, iters, taus)
    elif beta_estim == "M_beta":
        beta_z = update_beta_z_common(q_z, gamma_sb, model, sampling_model, 
                                      nb_data, k_trunc, beta_z_0,  
                                      neighborhood, nb_contrib, iters, beta_estim)
    elif beta_estim == "M_beta_concave":
        beta_z = update_beta_z_common(q_z, gamma_sb, model, sampling_model, 
                                      nb_data, k_trunc, beta_z_0, 
                                      neighborhood, nb_contrib, iters, beta_estim)        
    elif beta_estim == "MC_beta":
        beta_z = update_beta_z_mc(q_z, gamma_sb, model, sampling_model, nb_data, 
                                  k_trunc, beta_z_0, neighborhood, nb_contrib, 
                                  iters, mc_iters, beta_estim)        
    elif beta_estim == "MC_beta_concave":   
        beta_z = update_beta_z_mc(q_z, gamma_sb, model, sampling_model, nb_data, 
                                  k_trunc, beta_z_0, neighborhood, nb_contrib, 
                                  iters, mc_iters, beta_estim)                
    else:         
        msg = "Bad value for beta_estim: " + str(beta_estim)
        raise ValueError(msg)
    return beta_z


def update_beta_z_common(q_z, gamma_sb, model, sampling_model, nb_data, 
                         k_trunc, beta_z_0, neighborhood, nb_contrib, iters, 
                         beta_estim):

    if  beta_estim == "M_beta":
        beta_z = update_beta_z_gaus(q_z, gamma_sb, model, nb_data,
                                    k_trunc, beta_z_0,
                                    neighborhood, nb_contrib, gradient_beta)
    elif beta_estim == "M_beta_concave":
        beta_z = update_beta_z_gaus(q_z, gamma_sb, model, nb_data,
                                    k_trunc, beta_z_0,
                                    neighborhood, nb_contrib, gradient_beta_concave)
    else:         
        msg = "Bad value for beta_estim: " + str(beta_estim)
        raise ValueError(msg)           
    return beta_z

def update_beta_z_elbo(q_z, gamma_sb, model, sampling_model, nb_data, k_trunc, 
                       beta_z_0, neighborhood, nb_contrib, iters, taus):

    from scipy import inf
    
    res_val = inf
    improve = -1.
    
    min_beta, max_beta = init_update_beta_brent(gradient_beta, q_z, gamma_sb, nb_data, 
                                                k_trunc, neighborhood, 
                                                nb_contrib)
    
    minus_compute_z_tau_entropy = lambda beta_z, q_z, taus, neighborhood, nb_contrib: \
        -compute_z_tau_entropy(beta_z, q_z, taus, neighborhood, nb_contrib)
        
    try:        
        res_opt = optimize.minimize_scalar(minus_compute_z_tau_entropy, 
                                           args=(q_z, taus, neighborhood, 
                                                 nb_contrib), 
                                           method='brent',
                                           bracket=(min_beta, max_beta), 
                                           options={'maxiter':200})
        beta_z = res_opt.x
        res_val = res_opt.fun
        improve = -res_val - compute_z_tau_entropy(beta_z_0, q_z, taus, 
                                                   neighborhood, nb_contrib)
    except:
        print("Warning: No solution for M-beta step.")
        beta_z = beta_z_0
    
    if not(scipy.isfinite(res_val)) or improve < 0:
        print("Warning: No solution for M-beta step.")
        beta_z = beta_z_0
            
    return beta_z

def update_beta_z_mc(q_z, gamma_sb, model, sampling_model, nb_data, 
                     k_trunc, beta_z_0, neighborhood, nb_contrib, iters, 
                     mc_iters, beta_estim):    
    # Draw a sample from current variational approximation of tau posterior
    if mc_iters is None:
        mc_iters = 0
    if mc_iters <= 0:
        print("Warning: sample size is 0 in MC estimation of beta. Keeping previous value.")
        beta_z = beta_z_0
    else:        
        from scipy.stats import beta
        sim_tau = np.zeros((mc_iters, k_trunc), dtype=np.float64)
        # q_tau_k is a beta distribution with parameters gamma_sb[k,]

        for k in range(k_trunc-1):
            sim_tau[:,k] = beta.rvs(gamma_sb[k,0], gamma_sb[k,1], size=mc_iters)

        sim_tau[:,k_trunc-1] = 1.         
        if beta_estim == "MC_beta":
            gradient_beta_mc = lambda beta_z, q_z, gamma_sb, nb_data, k_trunc, \
                neighborhood, nb_contrib : \
                gradient_beta_mc_generic(beta_z, q_z, gamma_sb, nb_data, k_trunc, 
                                         neighborhood, nb_contrib, sim_tau)
        elif beta_estim == "MC_beta_concave":   
            gradient_beta_mc = lambda beta_z, q_z, gamma_sb, nb_data, k_trunc, \
                neighborhood, nb_contrib : \
                gradient_beta_mc_concave_generic(beta_z, q_z, gamma_sb, nb_data, 
                                                 k_trunc, neighborhood, 
                                                 nb_contrib, sim_tau)
            
        beta_z = update_beta_z_gaus(q_z, gamma_sb, model, nb_data,
                                    k_trunc, beta_z_0, neighborhood, nb_contrib, 
                                    gradient_beta_mc)                 

    return beta_z
   

def update_beta_z_specific(q_z, gamma_sb, model, sampling_model, nb_data, 
                           k_trunc, beta_z_0, neighborhood, nb_contrib, iters):

    #draw_gradient_beta(-10., 10., 1000, q_z, gamma_sb, nb_data, k_trunc,
    #                       beta_z_0, neighborhood,  nb_contrib, iters)
    
    if sampling_model == "gaussian":
                
        beta_z = update_beta_z_gaus(q_z, gamma_sb, model, nb_data,
                                    k_trunc, beta_z_0, neighborhood, nb_contrib, 
                                    gradient_beta)
    elif sampling_model == "poisson" or sampling_model == "binomial":
    
        beta_z =  update_beta_z_disease_mapping(q_z, gamma_sb, model, nb_data,
                            k_trunc, beta_z_0, neighborhood, nb_contrib)
    else:
        print("Warning: Please check the model name.")
        raise SystemExit
        
    return beta_z

def update_beta_z_gaus(q_z, gamma_sb, model, nb_data, k_trunc, beta_z_0,
                       neighborhood, nb_contrib, gradient_beta):

    beta_z = 0.0
    min_beta = -1.1
    max_beta = 4.5
    found = True
    # Fast initialization with dummy values
    if (model == 'dp-mrf') or (model == 'pyp-mrf'):
        try:
            res = None
            v1 = gradient_beta(min_beta, q_z, gamma_sb, nb_data, k_trunc, 
                               neighborhood, nb_contrib)
            v2 = gradient_beta(max_beta, q_z, gamma_sb, nb_data, k_trunc, 
                               neighborhood, nb_contrib)
            if v1 * v2 < 0:
               beta_z, res = optimize.brentq(gradient_beta, min_beta, max_beta,
                                             args=(q_z, gamma_sb, nb_data, 
                                              k_trunc, neighborhood, 
                                              nb_contrib), 
                                              maxiter=200, full_output=True)
        except ValueError:
            found = False
        if found and \
        ((res is None) or not(hasattr(res, 'converge')) or not(res.converge)):
            found = False
    
    # Search root in coarse grid
    if ((model == 'dp-mrf') or (model == 'pyp-mrf')) and not (found):
        flag, roots = find_multiple_roots(gradient_beta, min_beta, max_beta,
                                          args=(q_z, gamma_sb, nb_data, 
                                                k_trunc, neighborhood, nb_contrib), 
                                          maxiter=200, full_output=False, 
                                          opt=optimize.brentq, depth=4)
        if flag == 1:
            found = False
            beta_z = beta_z_0
        else:
            if scipy.isinf(roots).all() or scipy.isnan(roots).all():
                found = False
                beta_z = beta_z_0
            else:
                # Choose closest root to previous value
                m = np.argmin(abs(roots-beta_z_0))
                beta_z = roots[m]
                found = True

    # Search root in fine grid
    if ((model == 'dp-mrf') or (model == 'pyp-mrf')) and not(found):        
        min_beta, max_beta = init_update_beta_brent(gradient_beta, q_z, 
                                                    gamma_sb, nb_data, 
                                                    k_trunc, neighborhood, 
                                                    nb_contrib)
        flag, roots = find_multiple_roots(gradient_beta, min_beta, max_beta,
                                          args=(q_z, gamma_sb, nb_data, 
                                                k_trunc, neighborhood, nb_contrib), 
                                          maxiter=200, full_output=False, 
                                          opt=optimize.brentq, depth=10)
        if flag == 1:
            beta_z = beta_z_0
            found = False
        else:
            if scipy.isinf(roots).all() or scipy.isnan(roots).all():
                found = False
                beta_z = beta_z_0
            else:
                # Choose closest root to previous value
                m = np.argmin(abs(roots-beta_z_0))
                beta_z = roots[m]
                found = True
                
    if not(found):
        print("Warning: No solution for M-beta step")

    return beta_z

# Optimize beta in VBM-beta step through gradient descent algorithm
def update_beta_z_disease_mapping(q_z, gamma_sb, model, nb_data, k_trunc,
                                  beta_z_0, neighborhood, nb_contrib):

    min_beta, max_beta = init_update_beta_brent(gradient_beta, q_z, gamma_sb, 
                                                nb_data, k_trunc, neighborhood, 
                                                nb_contrib)
    
    if (model == 'dp-mrf') or (model == 'pyp-mrf'):
        cur_b = beta_z_0
        gf = 1e100
        # Learning rate
        rate = 1e-4
        precision = 1e-15
        delta_b = 1.
        max_iters = 1e5
        iters = 0 
    
        while delta_b > precision and abs(gf) > 1e-6 and iters < max_iters:
            
            gf = gradient_beta(cur_b, q_z, gamma_sb, nb_data, k_trunc, 
                               neighborhood, nb_contrib)
            assert(not np.isnan(gf))
            #Gradient descent
            prev_b = cur_b
            cur_b = cur_b + rate * gf  
        
            delta_b = abs(cur_b - prev_b)
            
            iters = iters + 1
            
        gf = gradient_beta(cur_b, q_z, gamma_sb, nb_data, k_trunc, 
                           neighborhood, nb_contrib)   
        print("nb_iters={} beta={} gradient_beta={}".format(iters, cur_b, gf))
        
        if abs(gf) < 1e-3:
            return cur_b 
        else:
            return beta_z_0

    else:
        return 0.0


def draw_gradient_beta_parts(a, b, n, q_z, gamma_sb, nb_data, k_trunc, 
                             beta_z_0, neighborhood, nb_contrib, iters):
    """
    Draw 2 additive components of gradient wrt. beta (M-step)
    """
    import matplotlib.pyplot as plt    
        
    betas = np.arange(a, b, (b-a)/n)
    g2_beta = np.zeros(betas.shape[0], dtype=np.float64)
    
    g1_beta = part1_gradient_beta(q_z, gamma_sb, nb_data, k_trunc,
                                  neighborhood, nb_contrib)*0.5
    
    for i,e in enumerate(betas):
        g2_beta[i] = part2_gradient_beta(e, q_z, gamma_sb, nb_data, k_trunc, 
                                         neighborhood, nb_contrib)*0.5 
    
    fig, ax = plt.subplots(1, 1, constrained_layout=True)
    fig.suptitle('part2_gradient_beta part1={}'.format(g1_beta), fontsize=16)
    ax.plot(betas, g2_beta, "--", betas, 
            np.repeat(g1_beta, betas.shape[0]), "r--")
    ax.plot([0])
    plt.savefig("part2_fig_{}".format(iters))
    plt.close(fig) 
    

def draw_gradient_beta(min_beta, max_beta, q_z, gamma_sb, beta_z_0, nb_data, 
                       k_trunc, neighborhood, nb_contrib, path, gradient_beta):
    """
    Draw gradient wrt. beta (M-step) + roots
    """
    import matplotlib.pyplot as plt    
    
    nb_points = 200
    betas = np.arange(min_beta, max_beta, (max_beta-min_beta)/nb_points)
    
    grad = lambda beta: \
        gradient_beta(beta, q_z, gamma_sb, nb_data, k_trunc, neighborhood, 
                      nb_contrib)
    
    flag, roots = find_multiple_roots(gradient_beta, min_beta, max_beta,
                                      args=(q_z, gamma_sb, nb_data, 
                                            k_trunc, neighborhood, nb_contrib), 
                                      maxiter=200, full_output=False, 
                                      opt=optimize.brentq, depth=10)
    if flag == 0:
        m = np.argmin(abs(roots-beta_z_0))
        min_root = roots[m]
    else:
        min_root = None
    
    vals = list(map(grad, betas))
    
    # Compute expected log \pi and taus
    # tau_tilde, log_pi_tilde = compute_log_pi_tilde(gamma_sb, k_trunc)

    # Compute expected taus and associated log_pi 
    tau_tilde, _ = compute_tau_tilde(gamma_sb, k_trunc)
    log_pi_tilde = compute_log_pi_tilde(tau_tilde, k_trunc)
    
    # Compute tau posterior stdevs
    from scipy.stats import beta
    from matplotlib import collections  as mc
    
    beta_sdevs = np.zeros([k_trunc], dtype=np.float64)
    sdev_lines = []
    for k in range(k_trunc-1):
        beta_sdevs[k] = beta.std(gamma_sb[k,0], gamma_sb[k,0])
        sdev_lines += [[(k, tau_tilde[k]-beta_sdevs[k]), \
                        (k, tau_tilde[k]+beta_sdevs[k])]]
    
    beta_sdevs[k_trunc-1] = 0
    sdev_lines += [[(k_trunc-1, tau_tilde[k_trunc-1]), \
                    (k_trunc-1, tau_tilde[k_trunc-1])]]
    
    plt.subplot(3, 1, 1)
    plt.plot(betas, vals)
    plt.title('Grad beta: ')
    plt.xlabel('beta')
    plt.ylabel('gradient')
    if len(roots) > 1:
        plt.vlines(roots, ymin=min(vals), ymax=max(vals), colors='b')
    if not(min_root is None):
        plt.vlines(min_root, ymin=min(vals), ymax=max(vals), colors='r')        
    ax = plt.subplot(3, 1, 2)
    plt.bar(range(k_trunc), tau_tilde, zorder=1)

    if debug:
        print("sdevs: " + str(sdev_lines))

    lc = mc.LineCollection(sdev_lines, colors='black', linewidths=2, zorder=2)
    ax.add_collection(lc)
    plt.xlabel('states')
    plt.ylabel('expected taus')
    plt.subplot(3, 1, 3)
    plt.bar(range(k_trunc), log_pi_tilde)
    plt.xlabel('states')
    plt.ylabel('expected log pi')
    plt.savefig(path+"_gradient.svg")
    plt.close()
    print("Saving file: " + path + "_gradient.svg")

def draw_gradient_beta_no_roots(min_beta, max_beta, q_z, gamma_sb, nb_data, 
                                k_trunc, neighborhood, nb_contrib, path, 
                                gradient_beta):
    """
    Draw gradient wrt. beta (M-step) + roots
    """
    import matplotlib.pyplot as plt    
    
    nb_points = 200
    betas = np.arange(min_beta, max_beta, (max_beta-min_beta)/nb_points)
    
    grad = lambda beta: \
        gradient_beta(beta, q_z, gamma_sb, nb_data, k_trunc, neighborhood, 
                      nb_contrib)
       
    vals = list(map(grad, betas))
    
    # Compute expected log \pi and taus

    # tau_tilde, log_pi_tilde = compute_log_pi_tilde(gamma_sb, k_trunc)
    
    # Compute expected taus and associated log_pi 
    tau_tilde, _ = compute_tau_tilde(gamma_sb, k_trunc)
    log_pi_tilde = compute_log_pi_tilde(tau_tilde, k_trunc)

    # Compute tau posterior stdevs
    from scipy.stats import beta
    from matplotlib import collections  as mc
    
    beta_sdevs = np.zeros([k_trunc], dtype=np.float64)
    sdev_lines = []
    for k in range(k_trunc-1):
        beta_sdevs[k] = beta.std(gamma_sb[k,0], gamma_sb[k,0])
        sdev_lines += [[(k, tau_tilde[k]-beta_sdevs[k]), \
                        (k, tau_tilde[k]+beta_sdevs[k])]]
    
    beta_sdevs[k_trunc-1] = 0
    sdev_lines += [[(k_trunc-1, tau_tilde[k_trunc-1]), \
                    (k_trunc-1, tau_tilde[k_trunc-1])]]

    plt.subplot(3, 1, 1)
    plt.plot(betas, vals)
    plt.title('Grad beta: ')
    plt.xlabel('beta')
    plt.ylabel('gradient')
    ax = plt.subplot(3, 1, 2)
    plt.bar(range(k_trunc), tau_tilde, zorder=1)
    lc = mc.LineCollection(sdev_lines, colors='black', linewidths=2, zorder=2)
    ax.add_collection(lc)
    plt.xlabel('states')
    plt.ylabel('expected taus')
    plt.subplot(3, 1, 3)
    plt.bar(range(k_trunc), log_pi_tilde)
    plt.xlabel('states')
    plt.ylabel('expected log pi')
    plt.savefig(path+"_gradient.svg")
    plt.close()
    print("Saving file: " + path + "_gradient.svg")
