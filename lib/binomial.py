import numpy as np
import numba as nb
import scipy.special as special
from distribution import Distribution
from vbem_utils_numba import normalize_exp_qz, init_q_z
# from poisson import init_stra_Kmean

class Binomial(Distribution):
    """
    TODO: complete doc, implement copy, init_method, etc.
    """

    def __init__ (self, var_name, dim=1, init_method="k_means_only"):
        """
        :Parameters:
            `var_name` (str) - name of variable in data file? (if any...)
            `nbinit` (int) - number of random initializations
            `dim` (int) -  data dimension
            `init_method` (str) - method used for initialization
                ("k_means_only" or "free_energy" - 10 runs of k-means then keep
                highest free energy)                    
        """
        self.dist_name = "binomial"
        self.var_name = var_name
        self.parameter = []
        self.prior = {"psi1": 0.0, "psi2": 0.0}
        self.dim = dim
        self.init_method = init_method

    def __str__(self):
        return "variable " + self.var_name + ": " + self.dist_name +" distribution" \
                + "\nparameters: " + str(self._parameter) \
                + "\n prior parameters: " + str(self.prior) \
                + "\n posterior distibution parameters: " + str(self.post_param)


    def init(self, data, nb_data, dim_data, k_trunc):
             
        
        #---------------- init p_k's and prior -------------------
        probs, labels = init_stra_Kmean(data, nb_data, k_trunc)
        psi1, psi2 = init_psi(probs, k_trunc)
        q_z = init_q_z(labels, nb_data, k_trunc)           

        #-----------Set fields---------------
        self.dim = dim_data
        self.parameter = probs
        self.prior["psi1"] = psi1
        self.prior["psi2"] = psi2

        #----------- Markov Random field parameters initialization -----------
        s1 = 1.4
        s2 = 1.0
        mean_sigma = 0.0
        beta_z_0 = 0.0

        return q_z, s1, s2, mean_sigma, beta_z_0
    
    
    def up_date_posterior(self, data, q_z, nk, nb_data, dim_data, k_trunc):
        """
        a: (nb_data, dim_data) array
		data[0,:] = y_j number of observed cases within cattle j
		data[1,:] = n_j cattle population size
	     q_z: (nb_data, k_trunc) array
		q_z[j,k] = P(z_j = k)
        """
        
        psi1 = self.prior["psi1"]
        psi2 = self.prior["psi2"]
        
        hat_1, hat_2 = up_date_beta_post(data, psi1, psi2, q_z, nb_data, k_trunc)
        self.prior["psi1"] = hat_1
        self.prior["psi2"] = hat_2
        self.parameter = up_date_probs(hat_1, hat_2, k_trunc)


    def expectation_likelihood(self, data, nb_data, dim_data, k_trunc):
        """
	return (nb_data, k_trunc) array
        """
        psi1 = self.prior["psi1"]
        psi2 = self.prior["psi2"]
        
        return expectation_log_bin(data, psi1, psi2, nb_data, dim_data, k_trunc)


    def convergence(self, model, delta_beta, delta_qz, threshold):
        
        if model == "dp" or model == "pyp":
            return delta_qz < threshold
        elif model == "dp-mrf" or model == "pyp-mrf":
            return (delta_qz < threshold) or delta_beta == 0   
        else:
            print("Warning: Please check the model name.")
            raise SystemExit


#######################################################################################
#			 utils functions
#######################################################################################


def init_psi(probs, k_trunc):
    
    psi1 = np.zeros(k_trunc, dtype=np.float64)
    psi2 = np.zeros(k_trunc, dtype=np.float64)   

    for k in range(k_trunc):    
        psi1[k] = 1.0
        psi2[k] = psi1[k] * (1-probs[k])/probs[k]
        
    return psi1, psi2


def init_q_z_MLE(psi1, psi2, data, nb_data, dim_data, k_trunc):
    
    # MLE part of qz when beta=0 and pi_k = 1/k_trunc
    qz_init = expectation_log_bin(data, psi1, psi2, nb_data, dim_data, k_trunc)
    qz_init = normalize_exp_qz(qz_init, nb_data, k_trunc)

    return qz_init


@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=True)
def up_date_beta_post(data, psi1, psi2, q_z, nb_data, k_trunc):

    hat_1 = np.zeros(k_trunc, dtype=np.float64) 
    hat_2 = np.zeros(k_trunc, dtype=np.float64)

    for k in range(k_trunc):
        
        sum_1 = 0.0
        sum_2 = 0.0
        for j in nb.prange(nb_data):
            y_j = data[j, 0]
            n_j = data[j, 1]
            sum_1 = sum_1 + y_j * q_z[j, k]	
            sum_2 = sum_2 + (n_j - y_j) * q_z[j, k]

        hat_1[k] = psi1[k] + sum_1
        hat_2[k] = psi2[k] + sum_2

    return hat_1, hat_2


@nb.jit(nopython=False, nogil=True, fastmath=True, parallel=True)
def up_date_probs(hat_1, hat_2, k_trunc):
	
    probs = np.zeros(k_trunc, dtype=np.float64)	
    
    for k in nb.prange(k_trunc):
        probs[k] = hat_1[k]/(hat_1[k] + hat_2[k])

    return probs

	
@nb.jit(nopython=False, nogil=True, fastmath=True, parallel=True)
def expectation_log_bin(data, psi1, psi2, nb_data, dim_data, k_trunc):

    exp = np.zeros((nb_data, k_trunc), dtype=np.float64)
	
    for j in nb.prange(nb_data):
        y_j = data[j, 0]
        n_j = data[j, 1] 	
        assert(y_j >= 0)
        assert(n_j > 0)

        for k in nb.prange(k_trunc):
            second_term = special.digamma(psi1[k] + psi2[k])
            exp_log_pk = special.digamma(psi1[k]) - second_term	
            exp_log_one_minus_pk = special.digamma(psi2[k]) - second_term
            exp[j, k] = y_j * exp_log_pk + (n_j - y_j) * exp_log_one_minus_pk 	

    return exp
