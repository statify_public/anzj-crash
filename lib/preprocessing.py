import pandas as pd
import numpy as np
from math import log




def get_data_and_neighbors(data_path, neighbors_path, dtype=np.int64):

    try:
        data = pd.read_csv(data_path, sep=",", dtype=dtype)
    except (ValueError, TypeError):
        data = pd.read_csv(data_path, sep="," , dtype=np.float64)
    neighbors = pd.read_csv(neighbors_path, sep=",", dtype=np.int64)
    """
    print("data {}, \n\n\nneighborhood {}".format(data, neighbors))
    """
	# convert dataframe to np.array, required by numbda which does not 
	# support pandas data frame
    data = data.values
    neighbors = neighbors.values
    
    """
    print("data {}, \n\n\nneighborhood {}".format(data, neighbors))
    """
    return data, neighbors

def check_neighbors(neighbors):
    """
    Check that neighbors is symmetric, non-reflexive    
    """
    nb_data = neighbors.shape[0]
    nc = {}    
    for n in range(nb_data):  
        local_list = neighbors[n, :]
        nc[n] = local_list[local_list > -1]
        msg = "Vertex " + str(n) + " is a neighbor of self"
        assert(not(n in nc[n])), msg
        
    for n in range(nb_data):  
        for nb in nc[n]:
            msg = "Asymmetry neighborhood in vertex:" + str((n, nb))
            assert(n in nc[nb]), msg
            
    return(nc)

def save_adjacency_matrix(data_path, neighbors, sparse=False):
    """
    Save adjacency matrix
    Replace data name suffix by .adj
    """
    import os
    import numpy as np
    path = os.path.dirname(data_path)
    bname = os.path.basename(data_path)
    nb_v = len(neighbors.keys())
    A = np.zeros((nb_v,nb_v), np.int64) # adjacency matrix
    
    path = os.path.join(path, bname[0:-4]+"_adj")
    print(path)
    
    for (k, v) in neighbors.items():
        A[k,v] = 1
    
    if sparse:
        # Save as scipy sparce matrix
        from scipy.sparse import coo_matrix, save_npz
        A = coo_matrix(A)
        save_npz(path, A)
    else:
        np.save(path, A)
    
    
def count_clusters(MAP_est, q_z, k_trunc, nb_data):
    
    h = entropy_qz(q_z, k_trunc, nb_data)
    
    return MAP_est, h
    

def entropy_qz(q_z, k_trunc, nb_data, iters=-1):

    H = np.zeros(nb_data, dtype=np.float64)
    for j in range(nb_data):
        aux = 0.0
        for k in range(k_trunc):
            if q_z[j,k] != 0.0:
                aux += q_z[j,k] * log(q_z[j,k])
        H[j] = -aux
    
    """
    fig, ax = plt.subplots(1, 1, constrained_layout=True, figsize=(20,100))
    fig.suptitle('heatmap', fontsize=16)
    max_qz = np.max(q_z, axis=1)
    indices = max_qz < 0.9
    ax = sns.heatmap(q_z[indices,:], vmin=0, vmax=1, square=True)
    
    ax.plot([0])
    plt.savefig("qz_iter_{}".format(iters))
    plt.close(fig) 
    """
    
    return H
    
def load_labels(file, k_trunc):
    """
    Load and check label file
    """
    labels = np.loadtxt(file, delimiter=",", dtype=np.int32)

    if len(labels.shape) != 1:
        msg = "Warning: wrong shape for initial labels. "
        msg += "Expecting numpy.array -like with shape (nb_data,)"
        print(msg)
        raise SystemExit

    if max(labels) >= k_trunc:
        msg = "Warning: maximum label exceeds k_trunc in initial labels."
        print(msg)
        raise SystemExit
    
    if len(set(labels)) < k_trunc:
        msg = "Warning: some label values are missing in initial label file."
        print(msg)

    return labels        