import numpy as np
import numba as nb
from scipy import io as sio
from scipy import special
from sklearn.metrics.cluster import adjusted_rand_score

################################################################################
#             Basic functions for performance evaluation PRI
################################################################################
# Read the ground truths given in BSDS500
def gt2array(input_gt):

    structure = sio.loadmat(input_gt)

    structure_gt = structure['groundTruth']

    _, nb_segs = np.shape(structure_gt)
    
    height, width = structure_gt[0, 0]['Segmentation'][0, 0].shape
    gt = np.zeros((height*width, nb_segs), dtype=np.int64)
    for s in range(nb_segs):
        gt[:, s] = structure_gt[0, s]['Segmentation'][0, 0].reshape(height*width)

    return gt, nb_segs, height, width

def label2array(input_label):

    labels = np.load(input_label)

    return labels

def compute_mean_ari(gt, labels, nb_segs):

    mean_ari = 0.0
    for s in range(nb_segs):
        mean_ari += adjusted_rand_score(labels, gt[s])

    return mean_ari/nb_segs

def rand_index_score(clus1, clus2):
    tp_plus_fp = special.comb(np.bincount(clus1), 2).sum()
    tp_plus_fn = special.comb(np.bincount(clus2), 2).sum()
    A = np.c_[(clus1, clus2)]
    tp = np.sum(special.comb(np.bincount(A[A[:, 0] == i, 1]), 2).sum() 
            for i in set(clus1))
    fp = tp_plus_fp - tp
    fn = tp_plus_fn - tp
    tn = special.comb(len(A), 2) - tp - fp - fn
    
    return (tp + tn) / (tp + fp + fn + tn)

def compute_pri(gt, labels, nb_segs, height, width):
    
    # nb_data = height*width
    pri = 0.0
    for s in range(nb_segs):
        pri += rand_index_score(np.transpose(labels[:, 0]), 
                                np.transpose(gt[:, s]))

    return pri/nb_segs

'''
@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=True)
def compute_pri(gt, labels, nb_segs, height, width):

    nb_data = height*width
    pri = 0.0
    for i in nb.prange(nb_data):
        for j in nb.prange(i+1, nb_data):
            pij = 0.0
            cij = 0.0
            for s in nb.prange(nb_segs):
                if gt[i, s] == gt[j, s]:
                    pij += 1.0
            pij = pij / nb_segs
            
            if labels[i, 0] == labels[j, 0]:
                cij = 1.0
            else:
                cij = 0.0
            
            pri += cij*pij + (1.0-cij)*(1.0-pij)

    return pri*2.0/(nb_data*(nb_data-1.0))
'''

###############################################################################
#      Pairwise probability matrix for finding optimal clustering (attempts)
###############################################################################
# Memory problem since the matrix dimensions are too huge to handle
@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=True)
def estimate_psm(q_z, nb_data, k_trunc):

    psmat = np.zeros((nb_data, nb_data), dtype=np.float64)
    for i in nb.prange(nb_data):
        for j in nb.prange(nb_data):
            if i == j:
                psmat[i, j] = 1.0
            else:
                for k in nb.prange(k_trunc):
                    psmat[i, j] += q_z[i, k] * q_z[j, k]
    
    return psmat


# Memory problem since the matrix dimensions are too huge to handle
@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=True)
def indicator_mu(m_niw, k1, k2, threshold):
    diff = 0.0
    for d in nb.prange(m_niw.shape[1]):
        diff += (m_niw[k1, d] - m_niw[k2, d]) ** 2
    if np.sqrt(diff) < threshold:
        return 1.0
    else:
        return 0.0


@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=True)
def estimate_psm_with_mu(q_z, m_niw, nb_data, k_trunc, threshold):

    psmat = np.zeros((nb_data, nb_data), dtype=np.float64)
    for i in nb.prange(nb_data):
        for j in nb.prange(nb_data):
            if i == j:
                psmat[i, j] = 1.0
            else:
                for k1 in nb.prange(k_trunc):
                    for k2 in nb.prange(k1+1, k_trunc):
                        if indicator_mu(m_niw, k1, k2, threshold) == 1.0:
                            psmat[i, j] += q_z[i, k1] * q_z[j, k2]
    
    return psmat
