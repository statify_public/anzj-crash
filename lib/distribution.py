#####################################################################################
#
#####################################################################################


class Distribution():
    """ 
    Collection of emission distributions.
    
    self.parameter corresponds to posterior means of parameters theta, 
        whose first axis is k_trunc
    self.prior corresponds to the prior parameters (updated in M step)
    
    :Todo:
        - make use and nature of self.parameter and self.post_param  more coherent
            through inherited classes
    """

    # Class attributes
        
        
    def __init__(self, nbinit):
        self.var_name = ""
        self.parameter = {}
        self.prior = {}
        self.post_param = {}
        self.nb_init = nbinit
                        
    def __str__(self):
        return self.var_name +" distribution" + "\nparameters:" + str(self.parameter) \
                + "\n prior parameters: " + str(self.prior) \
                + "\n posterior distibution: " + str(self.post_param)
    
    def convergence(self, deltas, threshold):       
        
        max_deltas = max(abs(deltas))
        return (max_deltas < threshold)
            
    def copy(self):
        """
        Copy current model
        """
        p = Distribution()
        p.var_name = self.var_name
        p.parameter = dict(self.parameter)
        p.prior = dict(self.prior)
        p.post_param = dict(self.post_param)

        return p
        
    def expectation_likelihood(self, data, nb_data, dim_data, k_trunc):
        """
        Compute expectations of log emission probability under (marginal) 
        variational distribution of lambda parameter given state z=k for every k.
        
        :Parameters:
          `data` (numpy.array) - data
          `nb_data` (int) - sample size
          `dim_data` (int) - data dimension
          `k_trunc` (int) - truncation values of nb clusters
          
        :Returns:
            [nb_data, k_trunc] np.array
    
        :Remark:
            Constant terms wrt states z may be omitted.

        """
        print("not implemented yet")

        
    def up_date_posterior(self, data, q_z, nk, nb_data, dim_data, k_trunc):
        """
        Update hyperparameters rho
        """
        print("not implemented yet")
        
    def up_date_prior(self, data, q_z, nk, nb_data, dim_data, k_trunc):
        print("not implemented yet")

    def entropy_theta(self):
        """
        Entropy of variational q_theta distribution 
        (up to constants wrt hyperparameters Phi)
        
        :Remark:
            In principle, this function could be implemented here by calling
            self.expectation_likelihood() in the inherited class and by summing
            the result over both axes.
        """
        print("not implemented yet")

   
