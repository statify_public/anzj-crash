#####################################################################################
#
#####################################################################################
import numpy as np
from binomial import Binomial
from poisson import Poisson
from gaussian import Gaussian 
# Nb PitManYor includes DP as particular cases with sigma=0.
from pitmanyor import PitManYor
from vbem_utils_numba import expectation_z, compute_q_z_tilde
from vbem_utils_numba import neighbour_contributions
from vbem_utils_numba import normalize_exp_qz, discrete_entropy#, update_beta_z
from potts_model import update_beta_z


class VBEM():
    """ 
    Implementation of VBEM algorithm for BNP HMRFs
    
    :Attributes:
        `sampling_model` - name of sampling distribution 
            (e.g., "gaussian", "poisson")
        `model` - name of variant (BNP, MRF, ...): dp, dp-mrf, pyp, pyp-mrf
        `k_trunc` - cluster truncation number
        `process` - a PitManYor object (representing BNP)
        `py` - a Distribution object (representing the sampling distribution)
    """

    # Initialization
    def __init__(self, sampling_model, model, k_trunc, nb_init, init_method,
                 init_file=""):
        """
        :Parameters:
            `sampling_model (str)` - sampling distribution to use 
                (e.g. "gaussian", "poisson")
            `model` (str) - dp, dp-mrf, pyp, pyp-mrf
            `k_trunc` (int) - cluster truncation number
            `nb_init` (int) -  number of trials in label initialization
            `init_method` (str) - method used for initialization
                ("k_means_only", "file" or "free_energy" - 10 runs of k-means 
                then keep highest free energy)            
            `init_file` (str) - path to file containing initial labels if 
                init_method is "file"
        """
        self.sampling_model = sampling_model
        self.model = model
        self.k_trunc = k_trunc
        self.process = PitManYor(k_trunc)
        self.init_method = init_method
        self.init_file = init_file
        
        if not(init_method in ["k_means_only", "free_energy", "file"]):
            msg = "Bad value for --init_method: " + str(init_method)
            msg += "; should be either k_means_only, free_energy or file"
            raise ValueError(msg)

        # Initialize the emission distribution
        # Should rather use a factory method, see
        # https://realpython.com/factory-method-python/#introducing-factory-method
        if sampling_model == "gaussian":
            self.py = Gaussian("y", nb_init, init_method=init_method, 
                               init_file=init_file) 
        elif sampling_model == "poisson":
            self.py = Poisson("y", nb_init, init_method=init_method, 
                               init_file=init_file)
        elif sampling_model == "binomial":
            self.py = Binomial("y", nb_init, init_method=init_method, 
                               init_file=init_file)
        else:
            raise ValueError("Error: check the sampling model")
        
                  
    def __str__(self):
        return "model " + self.model \
                + "\n sampling model: " + str(self.sampling_model) \
                + "\n truncation k: " + str(self.k_trunc) \
                + "\n initialization method: " + str(self.init_method)

    def copy(self, q_z, mean_alpha, mean_sigma, beta_z_0):
        """
        Copy current model and q_z, mean_alpha, mean_sigma, beta_z_0
        """
        vb = VBEM(self.sampling_model, self.model, self.k_trunc, 
                  self.py.nb_init, self.init_method, self.init_file)
        vb.py = self.py.copy()
        vb.process = self.process.copy()
        return (vb, q_z, mean_alpha, mean_sigma, beta_z_0)
        
    def elbo(self, data, q_z, beta_z, neighborhood, nb_contrib):
        """
        Compute elbo (up to constants wrt hyperparameters Phi and variational
        parameters)
        
        :Parameters:
            `data` ([nb_data, dim_data] np.array) - Sample
            `q_z` (([nb_data, k_trunc] np.array)) - variational approximation 
                of state distribution
            `beta_z` (float) - Current value of beta
            `neighborhood` (np.array) - matrix of neighbors 
              (-1 means no further neighbor)
            `nb_contrib` (np.array) - matrix of contributions of neighbors 
              to free energy              
        """
        # compute PY contribution to elbo: alpha and tau
        e = self.process.elbo(beta_z, q_z, neighborhood, nb_contrib)
        # state entropy
        e += discrete_entropy(q_z) 
        # q_theta-related entropy terms
        e += self.py.entropy_theta(data, q_z) 

        return(e)
        
    def replace(self, vb):
        
        """
        Copy vb model into self
        """
        self.sampling_model = vb.sampling_model
        self.model = vb.model
        self.k_trunc = vb.k_trunc
        self.py = vb.py.copy()
        self.process = vb.process.copy()

    def run(self, data, neighborhood, threshold, maxi, seed, nbinit, 
            alpha_estim, beta_estim, return_model, mc_nb_iters=None):
        """ 
        Run VBEM algorithm to estimate hyperparameters and compute variational
        posterior approximation
                
        :Parameters:
          `data` ([nb_data, dim_data] np.array) - observed data
          `neighborhood` (np.array) - matrix of neighbors 
              (-1 means no further neighbor)
          `threshold` (float) - Threshold on relative difference in q_z 
              used as stopping criterion
          `maxi` (int) - maximal number of iteration. If zero, save initial
              settings
          `seed` (float) - seed used for random number generation
          `nbinit` (int) - number of runs for state initialization
          `alpha_estim` (str) - method for alpha estimation 
              ("q_alpha" or "free_energy")
          `beta_estim` (str) - method for beta estimation 
              ("M_beta", "MC_beta" or "free_energy")
          `return_model` (str) - model returned at last iteration: "final" model 
              or model with highest free energy ("best_free_energy")
          `mc_nb_iters` (int) - number of Monte-Carlo iterations in  
             beta_estim = 'MC_beta'
              
        :Returns:
            `c_est` - MAP clustering
            `q_z` - Variational approximation q_z 
            `alpha_stored` - Expectations of BNP alpha parameter over iterations
            `sigma_stored` - Expectations of BNP sigma parameter over iterations
            `corr_as` - Estimated correlation of BNP alpha and sigma parameters 
                over iterations
            `beta_stored` - Sequence of estimated beta
            `delta_qz_stored`- Sequence of relative differences in q_z 
                over iterations
            `elbo_stored`- Sequence of elbo values over iterations
            `best_iter` - Iteration maximizing elbo
        
        :Remark:
            Except for parameter alpha, the variational parameters and estimates
            resulting from M steps are identical and stored in self. Variational 
            parameters for tau are stored in temporary variables and also in
            self.process.tau
        """
     
        # nb_data: number of data points, dim_data: data dimension
        (nb_data, dim_data) = np.shape(data)
        k_trunc = self.k_trunc
        model = self.model
        
        # Number of Monte-Carlo iteration in beta_estim = "MC_beta"
        default_mc_iter = 30
        if (mc_nb_iters is None) and \
            ((beta_estim == "MC_beta") or (beta_estim == "MC_beta_concave")):
            mc_iters = default_mc_iter
        elif (beta_estim != "MC_beta") and  (beta_estim != "MC_beta_concave"):
            mc_iters = None
        else:
            mc_iters = int(mc_nb_iters)
        #------------------- Initialization -----------------------                     
                        
        # Number of samples for importance sampling
        nb_samples = 10000
        
        # init sampling distribution parameters, q_z and Markov Random Field parmeters
        q_z, s1, s2, mean_sigma, beta_z_0 = self.py.init(data, nb_data, 
                                                         dim_data, k_trunc, 
                                                         seed, nbinit)
        
        mean_alpha = s1/s2
        mean_log_aps = 0.0
        corr_as = 0.0
        max_deltas = 0.0
        
	   # init process
        self.process.initialize(s1, s2, mean_sigma) 
           
        nk = q_z.sum(axis=0)
        print("Initial nk:", nk)
                
        # Check arguments not checked during first iteration
        if return_model != "final" and return_model != "best_free_energy":
            msg = "return_model: " + str(return_model) 
            msg += ": should be final or best_free_energy"
            raise ValueError(msg)

        # Store the inference results
        # Note that the lengths of delta_qz_stored, elbo_stored are one unit
        # shorter than the others.
        alpha_stored = np.array([mean_alpha], dtype=np.float64)
        sigma_stored = np.array([mean_sigma], dtype=np.float64)
        beta_stored = np.array([beta_z_0], dtype=np.float64)
        delta_qz_stored = np.array([], dtype=np.float64)
        elbo_stored = np.array([], dtype=np.float64)
        import math
        elbo_old = -nb_data * 1e10 * math.log(k_trunc)
        max_elbo = -nb_data * 1e10 * math.log(k_trunc)
        best_model = self, q_z, mean_alpha, mean_sigma, beta_z_0
        best_iter = 0 # iteration of best model

        # -------------------- iteration ----------------------
        
        if (model == 'dp-mrf' or model == 'pyp-mrf'):
                print("Smoothing activated.")

        # Start the VBEM algorithm
        iterate = (maxi > 0) # Stopping criterion
        iters = 0 # Nb iterations
        while iterate:

            q_z_old = np.copy(q_z)
            beta_z_0_old = beta_z_0
                
            #---------------------- VE-steps ------------
            # update tau's posterior 
            # gamma_sb are the variational parameters gamma of tau posteriors
            # Note that the other variational parameters are identical to M-step
            # estimates and thus stored in self. 
            # gamma_sb is also in self.process.tau
            gamma_sb = self.process.update_tau(mean_alpha, mean_sigma, nk, k_trunc)
            # E[log(tau)] and E[log(1-tau)]
            
            log_tau, log_one_minus_tau = self.process.compute_log_Tau()
            
            mean_alpha, mean_sigma, mean_log_aps = \
                self.process.compute_means(self.model, log_tau, 
                                           log_one_minus_tau, k_trunc, 
                                           nb_samples)
            
            exp_log = self.py.expectation_likelihood(data, nb_data, dim_data, k_trunc)

            # update clusters's labels 
            q_z = expectation_z(q_z, beta_z_0, exp_log, log_tau, 
                                log_one_minus_tau, nb_data, k_trunc, neighborhood)
            
            # Compute local_energy for multiple reuse
            nb_contrib = neighbour_contributions(q_z, nb_data, k_trunc, 
                                                 beta_z_0, neighborhood)
    
            # cluster counts
            nk = q_z.sum(axis=0)

            # update process parameters (s1,s2) of alpha variational posterior
            self.process.update_alpha(model, log_one_minus_tau, mean_alpha,
                                           mean_log_aps, k_trunc, alpha_estim)

            mean_alpha, mean_sigma, mean_log_aps = \
                self.process.compute_means(self.model, log_tau, 
                                           log_one_minus_tau, k_trunc, 
                                           nb_samples)
                        
            #----------------------check that everything goes right ----------
            assert(np.min(np.sum(q_z, axis=1)) >= 0.99)
            _q_z_tilde = compute_q_z_tilde(beta_z_0, q_z, gamma_sb, 
                                           nb_data, k_trunc, nb_contrib)
            # Mormalize q_z_tilde:
            _q_z_tilde = normalize_exp_qz(_q_z_tilde, nb_data, k_trunc)
            assert(np.min(np.sum(_q_z_tilde , axis=1)) >= 0.99)
            #-------------------------------------------------------------
           
            
            #------------------- VM-steps ------------
            
            # update distribution p(y|theta): hyperparameters rho
            self.py.update_posterior(data, q_z, nk, nb_data, dim_data, k_trunc)
            
            # update potts model
            beta_z_0 = update_beta_z(q_z, gamma_sb, model, self.sampling_model, 
                                     nb_data, k_trunc, beta_z_0, 
                                     neighborhood, nb_contrib, iters, mc_iters, 
                                     self.process.tau, beta_estim)
                        
            # --------------------------------------------------------------
            # Compute elbo
            elbo = self.elbo(data, q_z, beta_z_0, neighborhood, nb_contrib)

            # --------------------------------------------------------------
            
            # Relative difference in q_z
            delta_qz = np.linalg.norm(q_z-q_z_old) / np.linalg.norm(q_z_old)
            
            # Difference in estimated beta
            delta_beta = beta_z_0 - beta_z_0_old
            
            # Difference in free energy
            delta_elbo = elbo - elbo_old
            elbo_old = elbo
            
            if elbo > max_elbo:
                max_elbo = elbo
                best_model = self.copy(q_z, mean_alpha, mean_sigma, beta_z_0)
                best_iter = iters
            
            # Display each iteration
            print(iters+1, "nk:", nk)
            print(iters+1, "delta_qz=" + str(delta_qz))
            print(iters+1, "elbo=" + str(elbo))
            """
            print(iters+1, "delta_qz=" + str(delta_qz))
            print(iters+1, "alpha=" + str(mean_alpha))
            print(iters+1, "sigma=" + str(mean_sigma))
            print(iters+1, "beta=" + str(beta_z_0))
            print(iters+1, "delta_beta=" + str(delta_beta))
            """
            print("---------------------------------")
            
            alpha_stored = np.append(alpha_stored, mean_alpha)
            sigma_stored = np.append(sigma_stored, mean_sigma)
            beta_stored = np.append(beta_stored, beta_z_0)
            delta_qz_stored = np.append(delta_qz_stored, delta_qz)
            elbo_stored = np.append(elbo_stored, elbo)            

            # Stopping criterion
            deltas = np.array([delta_beta, delta_qz, delta_elbo])
            max_deltas = max(abs(deltas))
            if self.py.convergence(model, deltas, threshold):
                iterate = False
            
            iters += 1 
            if iters >= maxi:
                iterate = False
            
            """
            if self.sampling_model == "gaussian":
                if self.py.convergence(model, delta_beta, delta_qz, threshold):
                    break
            
            elif self.py.convergence(model, delta_beta, delta_qz, threshold):
                    break
            """
        if return_model == "best_free_energy":
            # Replace final model by the one with highest free energy
            vb, q_z, mean_alpha, mean_sigma, beta_z_0 = best_model
            self.replace(vb)
        else:
            # return model at last iteration$
            best_iter = iters-1

        # Determine optimal clustering using MAP
        
        c_est = np.argmax(q_z, axis=1)
        
        # Compute the correlation between alpha and sigma for PYP
        if model == 'pyp' or model == 'pyp-mrf':
            corr_as = self.process.corr_alpha_sigma_func(mean_alpha, mean_sigma)

        
        return (c_est, q_z, alpha_stored[1:], sigma_stored[1:], 
            corr_as, beta_stored[1:], delta_qz_stored, elbo_stored, best_iter,
            max_deltas)
	
	
