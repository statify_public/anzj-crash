import pandas as pd
import numpy as np
import numba as nb
from scipy.ndimage import convolve
from itertools import product, chain
from scipy.misc import ascent
from sklearn.externals.joblib import Parallel, delayed
from skimage.util import view_as_windows
from skimage.util import random_noise
from skimage.segmentation import slic
from skimage import io
from skimage import color
from skimage import exposure
from skimage import util

################################################################################
#                        Basic functions
################################################################################
def add_noise(input_img, noise_type, var):
    # Add noise to image
    image = io.imread(input_img)
    arr = image

    out = util.random_noise(arr, mode=noise_type, seed=None, clip=True, var=var)

    return out*255


def color2gray(input_img):

    arr = io.imread(input_img, as_gray=True)

    return arr*255


def im2gray(input_img):

    arr = io.imread(input_img)
    height, width = arr.shape
    channel = 1

    # Pixel filter
    '''
    threshold = 255
    for h in range(height):
        for w in range(width):
            if arr[h, w] > threshold:
                arr[h, w] = 255
    '''
    return arr, height, width, channel


def im2color(input_img):

    arr = io.imread(input_img)
    height, width, channel = arr.shape
    
    return arr, height, width, channel

    
def read_image(input_img, img_type):
    if img_type == "grayscale":
        return im2gray(input_img)
    elif img_type == "color":
        return im2color(input_img)
    else:
        print("Warning: please check the image type.")
        raise SystemExit


def im2col_sliding(arr, window_shape):

    aux = util.view_as_windows(arr, window_shape)

    return aux.reshape(-1, window_shape[0]*window_shape[1])

# Only for pixels
def neighbor_list(input_img):
    
    img = io.imread(input_img, as_gray=True)

    arr_pad = util.pad(img, pad_width=1, mode="reflect")

    height, width = arr_pad.shape
    # print(height, width)

    nb_windows_v = height - 2
    nb_windows_h = width - 2

    nb_sites = nb_windows_v * nb_windows_h
    index_vec = np.arange(nb_sites)

    index_mat = np.reshape(index_vec, (nb_windows_v, nb_windows_h))
    padded_mat = np.full((np.asarray(index_mat.shape) + [2, 2]), -1)
    padded_mat[1:-1, 1:-1] = index_mat

    # Number of neighbours = 8
    index_list = im2col_sliding(padded_mat, (3, 3))

    neighborhood = np.delete(index_list.astype(int), 4, axis=1)

    return neighborhood

# Only for superpixels based on the distance between two sites
@nb.jit(nopython=True, nogil=True, fastmath=True)
def find_neighbors(centers, nb_neighbors):
    nb_segs, _ = centers.shape
    distance_arr = np.zeros(nb_segs, dtype=np.float64)
    neighborhood = np.zeros((nb_segs, nb_neighbors), dtype=np.int64)
    for i in range(nb_segs):
        for j in range(nb_segs):
            distance_arr[j] = np.linalg.norm(centers[i]-centers[j])
        neighborhood[i] = np.argsort(distance_arr)[1:nb_neighbors+1]
    return neighborhood

################################################################################
#                       Extract texture features
################################################################################
def makeRFSfilters(radius=24, sigmas=[1, 2, 4], n_orientations=6):
    """ Generates filters for RFS filterbank.
    Parameters
    ----------
    radius : int, default 28
        radius of all filters. Size will be 2 * radius + 1
    sigmas : list of floats, default [1, 2, 4]
        define scales on which the filters will be computed
    n_orientations : int
        number of fractions the half-angle will be divided in
    Returns
    -------
    edge : ndarray (len(sigmas), n_orientations, 2*radius+1, 2*radius+1)
        Contains edge filters on different scales and orientations
    bar : ndarray (len(sigmas), n_orientations, 2*radius+1, 2*radius+1)
        Contains bar filters on different scales and orientations
    rot : ndarray (2, 2*radius+1, 2*radius+1)
        contains two rotation invariant filters, Gaussian and Laplacian of
        Gaussian
    """
    def make_gaussian_filter(x, sigma, order=0):
        if order > 2:
            raise ValueError("Only orders up to 2 are supported")
        # compute unnormalized Gaussian response
        response = np.exp(-x ** 2 / (2. * sigma ** 2))
        if order == 1:
            response = -response * x
        elif order == 2:
            response = response * (x ** 2 - sigma ** 2)
        # normalize
        response /= np.abs(response).sum()
        return response

    def makefilter(scale, phasey, pts, sup):
        gx = make_gaussian_filter(pts[0, :], sigma=3 * scale)
        gy = make_gaussian_filter(pts[1, :], sigma=scale, order=phasey)
        f = (gx * gy).reshape(sup, sup)
        # normalize
        f /= np.abs(f).sum()
        return f

    support = 2 * radius + 1
    x, y = np.mgrid[-radius:radius + 1, radius:-radius - 1:-1]
    orgpts = np.vstack([x.ravel(), y.ravel()])

    rot, edge, bar = [], [], []
    for sigma in sigmas:
        for orient in range(n_orientations):
            # Not 2pi as filters have symmetry
            angle = np.pi * orient / n_orientations
            c, s = np.cos(angle), np.sin(angle)
            rotpts = np.dot(np.array([[c, -s], [s, c]]), orgpts)
            edge.append(makefilter(sigma, 1, rotpts, support))
            bar.append(makefilter(sigma, 2, rotpts, support))
    length = np.sqrt(x ** 2 + y ** 2)
    rot.append(make_gaussian_filter(length, sigma=10))
    rot.append(make_gaussian_filter(length, sigma=10, order=2))

    # reshape rot and edge
    edge = np.asarray(edge)
    edge = edge.reshape(len(sigmas), n_orientations, support, support)
    bar = np.asarray(bar).reshape(edge.shape)
    rot = np.asarray(rot)[:, np.newaxis, :, :]
    return edge, bar, rot

def apply_filterbank(img_gray):
    # Apply MR8 filter to the grayscale image and return a Nx8 matrix
    edge, bar, rot = makeRFSfilters()
    filterbank = chain(edge, bar, rot)
    reponses = []
    for battery in filterbank:
        # response = [convolve(img, filt) for filt in battery]
        response = Parallel(n_jobs=10)(delayed(convolve)(img_gray, filt) 
                            for filt in battery)
        max_response = np.max(response, axis=0)
        reponses.append(max_response)
        # print("battery finished")
    
    height, width = reponses[0].shape
    nb_data = height*width
    mr8_feature = np.empty((nb_data, 0), dtype=np.float64)
    for res in reponses:
        mr8_feature = np.hstack((mr8_feature, res.reshape((nb_data, 1))))

    return mr8_feature

################################################################################
#                       Prepare feature vectors
################################################################################
def im2pixel(input_img, img_type, texture):

    arr_pixel, height, width, channel = read_image(input_img, img_type)
    nb_data = height*width
    nb_features = channel
    if texture == "yes":
        nb_features = channel + 8
    features = np.zeros((nb_data, nb_features), dtype=np.float64)
    color_feature = np.zeros((nb_data, channel), dtype=np.float64)
    
    # Trivial presegments
    presegments = np.arange(nb_data)
    presegments = presegments.reshape((height, width))
    
    # Color features with or without texture features
    if img_type == "grayscale":
        color_feature = np.reshape(arr_pixel, (nb_data, channel))
        if texture == "yes":
            texture_feature = apply_filterbank(np.float64(arr_pixel))
            features = np.hstack((color_feature, texture_feature))
        elif texture == "no":
            features = color_feature
        else:
            print("Warning: please check the option for texture extraction.")
            raise SystemExit
    elif img_type == "color":
        arr_pixel = color.rgb2hsv(arr_pixel) * 255
        color_feature = np.reshape(arr_pixel, (nb_data, channel))
        if texture == "yes":
            texture_feature = apply_filterbank(color2gray(input_img))
            features = np.hstack((color_feature, texture_feature))
        elif texture == "no":
            features = color_feature
        else:
            print("Warning: please check the option for texture extraction.")
            raise SystemExit
    else:
        print("Warning: please check the image type.")
        raise SystemExit
    
    # Pixel centers
    centers = np.array(list(np.nonzero(presegments+1)), dtype=np.float64)
    centers = centers.T
    
    # Color features with or without texture features
    if img_type == "grayscale":
        color_feature = np.reshape(arr_pixel, (nb_data, channel))
        if texture == "yes":
            texture_feature = apply_filterbank(np.float64(arr_pixel))
            features = np.hstack((color_feature, texture_feature))
        elif texture == "no":
            features = color_feature
        else:
            print("Warning: please check the option for texture extraction.")
            raise SystemExit
    elif img_type == "color":
        arr_pixel = color.rgb2hsv(arr_pixel) * 255
        color_feature = np.reshape(arr_pixel, (nb_data, channel))
        if texture == "yes":
            texture_feature = apply_filterbank(color2gray(input_img))
            features = np.hstack((color_feature, texture_feature))
        elif texture == "no":
            features = color_feature
        else:
            print("Warning: please check the option for texture extraction.")
            raise SystemExit
    else:
        print("Warning: please check the image type.")
        raise SystemExit

    arr_features = features

    return np.float64(arr_features), presegments, centers


def im2superpixel(input_img, img_type, texture, nb_superpixels):
    
    arr_pixel, height, width, channel = read_image(input_img, img_type)
    nb_data = height*width
    nb_features = channel
    if texture == "yes":
        nb_features = channel + 8
    features = np.zeros((nb_data, nb_features), dtype=np.float64)
    color_feature = np.zeros((nb_data, channel), dtype=np.float64)
    '''
    # Modify the image contrast
    pmin, pmax = np.percentile(arr_pixel, (2, 98))
    arr_pixel = exposure.rescale_intensity(arr_pixel, 
                                        in_range=(pmin, pmax))
    '''
    # SLIC presegments
    aux = np.array([])
    if img_type == "grayscale":
        aux = np.dstack([arr_pixel, arr_pixel, arr_pixel])
    elif img_type == "color":
        aux = arr_pixel
    else:
        print("Warning: please check the image type.")
        raise SystemExit
    
    presegments = slic(np.float64(aux), n_segments=nb_superpixels, 
                            convert2lab=True, slic_zero=True)

    presegments_ids = np.unique(presegments)
    nb_segs = len(presegments_ids)

    # Color features with or without texture features
    if img_type == "grayscale":
        color_feature = np.reshape(arr_pixel, (nb_data, channel))
        if texture == "yes":
            texture_feature = apply_filterbank(np.float64(arr_pixel))
            features = np.hstack((color_feature, texture_feature))
        elif texture == "no":
            features = color_feature
        else:
            print("Warning: please check the option for texture extraction.")
            raise SystemExit
    elif img_type == "color":
        arr_pixel = color.rgb2hsv(arr_pixel) * 255
        color_feature = np.reshape(arr_pixel, (nb_data, channel))
        if texture == "yes":
            texture_feature = apply_filterbank(color2gray(input_img))
            features = np.hstack((color_feature, texture_feature))
        elif texture == "no":
            features = color_feature
        else:
            print("Warning: please check the option for texture extraction.")
            raise SystemExit
    else:
        print("Warning: please check the image type.")
        raise SystemExit

    # Superpixel centers
    centers = np.empty((0, 2), dtype=np.float64)

    # Superpixel features
    arr_features = np.zeros((nb_segs, nb_features), dtype=np.float64)
    idx = 0
    for i in presegments_ids:
        indexes_seg = np.nonzero(presegments == i)
        center = np.mean(indexes_seg, axis=1)
        centers = np.vstack((centers, center.reshape((1, 2))))
        # Calculate mean feature values for each superpixel
        aux = features.reshape((height, width, nb_features))[indexes_seg]
        arr_features[idx, :] = np.mean(aux, axis=0)
        idx = idx + 1

    return np.float64(arr_features), presegments, centers

################################################################################
#                        Miscellaneous
################################################################################
# Convert the final result into usual pixel values [0, 255]
def image_restored(input_img, img_type, feature_type, texture, 
                    presegments, m_niw, c_est):

    arr, height, width, channel = read_image(input_img, img_type)
    arr = np.float64(arr)
    labels = presegments
    nb_data = height*width

    if feature_type == "px":
        if texture == "yes":
            arr = m_niw[c_est.flatten()][:, :-8]
        elif texture == "no":
            arr = m_niw[c_est.flatten()]
        else:
            print("Warning: please check the option for texture extraction.")
            raise SystemExit
        labels = c_est
    elif feature_type == "spx":
        presegments_ids = np.unique(presegments)
        idx = 0
        for i in presegments_ids:
            indexes_seg = np.nonzero(presegments == i)
            if texture == "yes":
                arr[indexes_seg] = m_niw[c_est.flatten()][idx, :-8]
            elif texture == "no":
                arr[indexes_seg] = m_niw[c_est.flatten()][idx]
            else:
                print("Warning: please check the option for texture \
                        extraction.")
                raise SystemExit
            labels[indexes_seg] = c_est[idx]
            idx = idx + 1
    else:
        print("Warning: please check the feature type.")
        raise SystemExit
    
    if img_type == "color":
        arr = color.hsv2rgb(arr/255) * 255
    arr = arr.reshape((nb_data, channel))
    labels = labels.reshape((nb_data, 1))

    return  np.uint8(arr), labels


def preprocess_data_rats(input_csv):

    df = pd.read_csv(input_csv, sep=",", header=0)

    data = df[df.columns[3:11]].values
    neighborhood = df[df.columns[15:23]].values

    return data.astype(np.float64), neighborhood.astype(np.int64)
