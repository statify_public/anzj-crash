from distribution import Distribution
from vbem_utils_numba import init_by_kmeans, compute_weighted_mean, \
    compute_weighted_cov, compute_niw, expectation_log_gauss, \
    compute_exp_inv_psi, discrete_entropy, update_niw, \
    update_gamma_sb, compute_pi_tau_entropy

from preprocessing import load_labels

class Gaussian(Distribution):
    """ 
    Collection of Gaussian emission distributions.
    
    self.parameter corresponds to parameters theta, whose first axis is k_trunc
    """

    def __init__(self, var_name, nbinit, dim=1, init_method="k_means_only", 
                 init_file=""):
        """
        :Parameters:
            `var_name` (str) - name of variable in data file? (if any...)
            `nbinit` (int) - number of random initializations
            `dim` (int) -  data dimension
            `init_method` (str) - method used for initialization
                ("k_means_only" or "free_energy" - 10 runs of k-means then keep
                highest free energy)              
            `init_file` (str) - path to file containing initial labels if 
                init_method is "file"                
        """
        # Set nbinit!: number of initializations of the Kmeans algorithm used in order
        # to find an initial partition

        super(Gaussian, self).__init__(nbinit)

        self.var_name = var_name
        self.parameter = {"mean": 0.0, "Sigma": 0.0}
        self.prior = {"nu": 0.0, "psi": 0.0, "m": 0.0, "lambda": 0.0}
        self.dim = dim 	# dim-dimensional normal distribution	
        self.dist_name = "gaussian"
        self.init_method = init_method
        self.init_file = init_file
        
    def __str__(self):
        return "variable " + self.var_name + ": " + self.dist_name +" distribution" \
                + "\nparameters: " + str(self.parameter) \
                + "\n prior parameters: " + str(self.prior)

    def copy(self):
        """
        Copy current model
        """
        p = Gaussian(self.var_name, self.nb_init, self.dim)
        p.parameter = dict(self.parameter)
        p.prior = dict(self.prior)
        p.dim = self.dim
        p.dist_name = self.dist_name     
        p.init_method = self.init_method
        p.init_file = self.init_file

        return p
    
    def init(self, data, nb_data, dim_data, k_trunc, seed, nbinit):
        
        import numpy
        # seet seed
        numpy.random.seed(seed)
        # Initialize variational q_z by k-means and update normal 
        # inverse-Wishart parameters
	   # Markov Random field parameters initialization
        s1 = 1.0
        s2 = 200.0/k_trunc
        mean_sigma = 0.0
        beta_z_0 = 0.0
        
        if self.init_method == "k_means_only":            
            m_niw_0, lambda_niw_0, psi_niw_0, nu_niw_0, q_z = \
                k_means_init(data, nb_data, dim_data, k_trunc, seed, nbinit, 
                             labels=None)
        elif self.init_method == "free_energy":
            # initial q_z
            nb_kmeans = 10
            m_niw_0, lambda_niw_0, psi_niw_0, nu_niw_0, q_z_current = \
                k_means_init(data, nb_data, dim_data, k_trunc, seed, nb_kmeans, 
                             labels=None)
            # Compute free energy for current q_z
            elbo_current = discrete_entropy(q_z_current)
            elbo_current += compute_entropy_theta(data, q_z_current, m_niw_0, 
                                                  lambda_niw_0,  psi_niw_0, 
                                                  nu_niw_0)
            nk = q_z_current.sum(axis=0)
            mean_alpha  = s1 / s2
            nk = q_z_current.sum(axis=0)
            gamma_sb = update_gamma_sb(nk, mean_alpha, mean_sigma, k_trunc)
            nb_data, k_trunc, nk, entropy = \
                compute_pi_tau_entropy(beta_z_0, q_z_current, gamma_sb)
            elbo_current += entropy
            elbo_best = elbo_current
            q_z = np.copy(q_z_current)
            for k in range(nbinit-1):
                m_niw_0, lambda_niw_0, psi_niw_0, nu_niw_0, q_z_current = \
                    k_means_init(data, nb_data, dim_data, k_trunc, seed, nb_kmeans, 
                             labels=None)
                # Compute free energy for current q_z
                elbo_current = discrete_entropy(q_z_current)
                elbo_current += compute_entropy_theta(data, q_z_current, m_niw_0, 
                                                      lambda_niw_0,  psi_niw_0, 
                                                      nu_niw_0)
                nk = q_z_current.sum(axis=0)
                gamma_sb = update_gamma_sb(nk, mean_alpha, mean_sigma, k_trunc)
                nb_data, k_trunc, nk, entropy = \
                    compute_pi_tau_entropy(beta_z_0, q_z_current, gamma_sb)
                elbo_current += entropy
                # Keep q_z maximizing free energy / elbo
                if elbo_best < elbo_current:
                    elbo_best = elbo_current
                    q_z = np.copy(q_z_current)
        elif self.init_method == "file":
            labels =  load_labels(self.init_file, k_trunc)
            m_niw_0, lambda_niw_0, psi_niw_0, nu_niw_0, q_z = \
                k_means_init(data, nb_data, dim_data, k_trunc, seed, nbinit, 
                             labels=labels)
        
        self.dim = dim_data
        self.prior["m"] = m_niw_0 
        self.prior["psi"] = psi_niw_0
        self.prior["nu"] = nu_niw_0
        self.prior["lambda"] = lambda_niw_0
                 
        return q_z, s1, s2, mean_sigma, beta_z_0
                       
    def update_posterior(self, data, q_z, nk, nb_data, dim_data, k_trunc):
        """
        Update hyperparameters rho
        
        :Remark: 
            dim_data is known by self and should not be passed as an argument
        """
        m_niw_0 = self.prior["m"]
        lambda_niw_0 = self.prior["lambda"]
        psi_niw_0 = self.prior["psi"]
        nu_niw_0  = self.prior["nu"]
        weighted_mean = compute_weighted_mean(q_z, data, nk, nb_data, 
                                              dim_data, k_trunc)
        weighted_cov = compute_weighted_cov(q_z, data, weighted_mean, 
                                            nb_data, dim_data, k_trunc)
        m_niw, psi_niw, lambda_niw, nu_niw = compute_niw(nk, weighted_mean,
                                                     weighted_cov,
                                                     m_niw_0, lambda_niw_0,
                                                     psi_niw_0, nu_niw_0,
                                                     dim_data, k_trunc)
    
        self.prior["m"] = m_niw
        self.prior["lambda"] = lambda_niw
        self.prior["psi"] = psi_niw
        self.prior["nu"] = nu_niw
            
     
        self.update_theta_posterior_mean()
            
    def update_theta_posterior_mean(self):
        """
        Update self.parameter
        """
        k_trunc = len(self.prior["lambda"])

        self.parameter = update_m_sigma(self.prior["m"], self.prior["lambda"], 
                                        self.prior["psi"], self.prior["nu"],
                                        k_trunc)
  

    def expectation_likelihood(self, data, nb_data, dim_data, k_trunc):
        """
        Compute -0.5 * expectation of log covariance determinant 
        for Gaussian distributions under variational theta* distribution.
        
        :Parameters:
          `data` (numpy.array) - data
          `nb_data` (int) - sample size
          `dim_data` (int) - data dimension
          `k_trunc` (int) - truncation values of nb clusters
          
        :Returns:
            [1, k_trunc] np.array
    
        :Remark:
            Constant terms wrt states z may be omitted.
            Dim_data is known by self and should not be passed as an argument

        """        
        m_niw = self.prior["m"]
        lambda_niw = self.prior["lambda"]
        psi_niw = self.prior["psi"]
        nu_niw  = self.prior["nu"]
        e = expectation_log_gauss(data, m_niw, lambda_niw, psi_niw, nu_niw,
                                nb_data, dim_data, k_trunc)
        
        return e

    def convergence(self, model, deltas, threshold):       
        
        delta_beta, delta_qz, delta_elbo = deltas
        if model == "dp" or model == "pyp":
            return delta_qz < threshold
        elif model == "dp-mrf" or model == "pyp-mrf":
            return super(Gaussian, self).convergence(deltas, threshold)
        else:
            print("Warning: Please check the model name.")
            raise SystemExit

    def entropy_theta(self, data, q_z):
        """
        Contribution of variational q_theta distribution to entropy
        (up to constants wrt hyperparameters Phi and variational parameters)

        :Parameters:
          `data` (numpy.array) - data
          `q_z` (([nb_data, k_trunc] np.array)) - variational approximation 
              of state distribution       
        """
        m_niw = self.prior["m"]
        lambda_niw = self.prior["lambda"]
        psi_niw = self.prior["psi"]
        nu_niw  = self.prior["nu"]
        e = compute_entropy_theta(data, q_z, m_niw, lambda_niw, 
                                  psi_niw, nu_niw)
        
        return (e)

import numpy as np
import numba as nb
import math
from scipy import special

@nb.jit(nopython=False, parallel=True, nogil=True)
def compute_entropy_theta(data, q_z, m_niw, lambda_niw, psi_niw, nu_niw):
    """
        Contribution of variational q_theta distribution to entropy
        (up to constants wrt hyperparameters Phi and variational parameters)
        
        :Parameters:
            `data` (np.array) - data
            `q_z` (([nb_data, k_trunc] np.array)) - variational approximation 
            of state distribution    
    """
    e = 0.
    
    k_trunc = lambda_niw.shape[0]
    
    nb_data = data.shape[0]
    dim_data = data.shape[1]
    nk = q_z.sum(axis=0)
    # Includes + dim_data / lambda_niw[k]
    inv_psi = compute_exp_inv_psi(data, m_niw, lambda_niw, psi_niw, 
                                  nu_niw, nb_data, dim_data, k_trunc)
    inv_psi = np.multiply(inv_psi, q_z)
    for k in range(k_trunc):
        psi_d = 0. # mutltivariate digamma function
        for d in range(dim_data):
            psi_d += special.digamma((nu_niw[k] + 1 - d)/2)
        e += nk[k] * (-math.log(np.linalg.det(psi_niw[k])) + \
               dim_data * math.log(2) + psi_d)
    e = e - inv_psi.sum()    
    e = 0.5 * e
    
    return(e)
                
def k_means_init(data, nb_data, dim_data, k_trunc, seed, nbinit, 
                labels=None):
    """
    Initialization of hyperparameters by opencv k-means
    """
    m_niw_0 = np.zeros((k_trunc, dim_data), dtype=np.float64)
    psi_niw_0 = np.zeros((k_trunc, dim_data, dim_data), dtype=np.float64)
    psi_niw_0[:] = np.eye(dim_data, dtype=np.float64) * 1000.0
    nu_niw_0 = np.ones(k_trunc, dtype=np.float64) * dim_data
    lambda_niw_0 = np.ones(k_trunc, dtype=np.float64)
         
    q_z, nk, labels = init_by_kmeans(data, nb_data, dim_data, k_trunc, 
                                     nbinit, seed, labels)
     
    m_niw, psi_niw, lambda_niw, nu_niw = update_niw(data, q_z, nk,
                                                    m_niw_0, lambda_niw_0,
                                                    psi_niw_0, nu_niw_0,
                                                    nb_data, dim_data,
                                                    k_trunc)
    
    return m_niw, lambda_niw, psi_niw, nu_niw, q_z
   
def update_m_sigma(m, l, psi, nu, k_trunc):
    """
    Update posterior means
    """
    
    res = {}
    
    dim_data = m[0].shape[0]
    
    mean = m
    sigma = np.copy(psi)
    
    for k in range(k_trunc):
        sigma[k] = psi[k] / (nu[k] - dim_data - 1)

    
    res["mean"] = mean
    res["sigma"] = sigma
    
    
    return res
