#!/usr/bin/env python
#-*- coding: utf-8 -*-
"""vbem_utils_numba module

.. module:: vbem_utils_numba
    :synopsis: A module dedicated to VBEM steps relying on numba


.. topic:: vbem_utils_numba.py summary

    A module dedicated to VBEM steps relying on numba (acceleration by 
    automatic parallelization)

    :Code status: in development
    :Documentation status: to be completed
    :Author: 
    :Revision: $Id: vbem_utils_numba.py$
    :Usage: >>> from vbem_utils_numba import *
 
"""
import numpy as np
import numba as nb
from numba import prange # Note that prange will not work in jupyter notebooks.
from scipy import stats
from scipy import special
from scipy import optimize
import math
from sklearn.cluster import KMeans, MeanShift, estimate_bandwidth

# may work on some images.
# 'safe' 'threadsafe' 'omp' would not

# 'workqueue''forksafe' would work
# nb.config.THREADING_LAYER = 'forksafe' 
# nb.config.THREADING_LAYER = 'workqueue'


#from potts_model import draw_gradient_beta
###############################################################################
#                        Functions for expectation steps
###############################################################################
# Compute the Potts part
@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=True)
def neighbour_contributions(q_z, nb_data, k_trunc, beta_z, neighborhood):
    """
    Mean-field (log, unnormalized) approximation of joint distribution 
    for joint variational q_z: contributions of pairs
            
    :Parameters:
      `q_z` ([nb_data, k_trunc] np.array) - value of q_z at previous iteration
      `nb_data` (int) - Sample size
      `k_trunc` (int) - Truncation values of nb clusters
      `beta_z` (float) - Current value of beta
      `neighborhood` (np.array) - matrix of neighbors 
          (-1 means no further neighbor)
          
    :Returns:
        Contributions of pairs to mean-field (log, unnormalized) 
        approximation of joint distribution for joint variational q_z 
        ([nb_data, k_trunc] np.array)

    """
    local_energy = np.zeros((nb_data, k_trunc), dtype=np.float64)
    for n in range(nb_data):
        local_list = neighborhood[n, :]
        neighbors = local_list[local_list > -1]
        for k in range(k_trunc):
            aux = 0.0
            for i in range(len(neighbors)):
                aux += q_z[neighbors[i], k]
            local_energy[n, k] = aux

    return local_energy

@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=True)
def pairwise_energy(beta_z, nb_contrib):
    """
    Mean-field (log, unnormalized) approximation of joint distribution 
    for joint variational q_z: contributions of pairs * beta
            
    :Parameters:
      `q_z` ([nb_data, k_trunc] np.array) - value of q_z at previous iteration
      `nb_data` (int) - Sample size
      `k_trunc` (int) - Truncation values of nb clusters
      `beta_z` (float) - Current value of beta
      `neighborhood` (np.array) - matrix of neighbors 
          (-1 means no further neighbor)
          
    :Returns:
        Contributions of pairs * beta to mean-field (log, unnormalized) 
        approximation of joint distribution for joint variational q_z 
        ([nb_data, k_trunc] np.array)

    """
    return beta_z * nb_contrib

@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def compute_weighted_mean(q_z, data, nk, nb_data, dim_data, k_trunc):
    """
    Update hyperparameters for normal inverse wishart (niw): weighted sample mean
    """
    weighted_mean = np.zeros((k_trunc, dim_data), dtype=np.float64)
    for k in nb.prange(k_trunc):
        for d in nb.prange(dim_data):
            for n in nb.prange(nb_data):
                weighted_mean[k, d] += q_z[n, k] * data[n, d]
            # safe division:
            if nk[k] > 0.0:
                weighted_mean[k, d] = weighted_mean[k, d] / nk[k]

    return weighted_mean


@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def compute_weighted_cov(q_z, data, weighted_mean, nb_data, dim_data, k_trunc):
    """
    Update hyperparameters for normal inverse wishart (niw): weighted sample cov
    """
    
    weighted_cov = np.zeros((k_trunc, dim_data, dim_data), dtype=np.float64)
    for k in range(k_trunc):
        for n in range(nb_data):
            for d1 in range(dim_data):
                for d2 in range(dim_data):
                    cov_element = (data[n, d1] - weighted_mean[k, d1]) \
                                * (data[n, d2] - weighted_mean[k, d2])
                    weighted_cov[k, d1, d2] += q_z[n, k] * cov_element
    
    return weighted_cov


@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def compute_niw(nk, weighted_mean, weighted_cov, 
                m_niw_0, lambda_niw_0, psi_niw_0, nu_niw_0, 
                dim_data, k_trunc):
    """
    VE-theta* step
    Compute updated hyperparameters in Gaussian distributions with
    normal-inverse-Wishart priors
    
    :Parameters:
      `nk` ([1,k_trunc] np.array) - Sums of variational posteriors 
          of cluster variables z
      `weighted_mean` ([k_trunc, dim_data] np.array) - Sample mean 
          with q_z weights
      `weighted_cov` ([k_trunc, dim_data, dim_data] np.array) - Sample
          covariance with q_z weights
      `m_niw_0` (numpy.array) - previous location parameters 
          of normal inverse Wishart
      `lambda_niw_0` (numpy.array) - previous precision parameters 
          of normal inverse Wishart
      `psi_niw_0` (numpy.array) - previous scale parameters of inverse Wishart
      `nu_niw_0` (numpy.array) - previous df parameters of inverse Wishart
      `dim_data` (int) - data dimension
      `k_trunc` (int) - truncation values of nb clusters
      
    :Returns:
        Updated parameters m_niw, psi_niw, lambda_niw, nu_niw
      
    """    
    psi_niw = np.zeros((k_trunc, dim_data, dim_data), dtype=np.float64)
    m_niw = np.zeros((k_trunc, dim_data), dtype=np.float64)
    lambda_niw = np.zeros(k_trunc, dtype=np.float64)
    nu_niw = np.zeros(k_trunc, dtype=np.float64)

    for k in range(k_trunc):
        lambda_niw[k] = lambda_niw_0[k] + nk[k]
        nu_niw[k] = nu_niw_0[k] + nk[k]
        for d1 in range(dim_data):
            for d2 in range(dim_data):
                psi_element = (m_niw_0[k, d1] - weighted_mean[k, d1]) \
                            * (m_niw_0[k, d2] - weighted_mean[k, d2])
                psi_niw[k, d1, d2] = psi_niw_0[k, d1, d2] \
                                + weighted_cov[k, d1, d2] \
                                + (lambda_niw_0[k]*nk[k] / lambda_niw[k]) \
                                * psi_element
        
            m_niw[k, d1] = (lambda_niw_0[k]*m_niw_0[k, d1]
                        + nk[k]*weighted_mean[k, d1]) / lambda_niw[k]

    return m_niw, psi_niw, lambda_niw, nu_niw


def update_niw(data, q_z, nk, m_niw_0, lambda_niw_0, psi_niw_0, nu_niw_0,
               nb_data, dim_data, k_trunc):

    """
    VE-theta* step
    Update hyperparameters in Gaussian distributions with
    normal-inverse-Wishart priors
    
    :Parameters:
      `data ([nb_data, dim_data] np.array) - Sample
      `q_z` ([nb_data, k_trunc] np.array) - variational q_z distribution 
          at previous iteration
      `nk` ([1,k_trunc] np.array) - Sums of variational posteriors 
          of cluster variables z
      `m_niw_0` (numpy.array) - previous location parameters 
          of normal inverse Wishart
      `lambda_niw_0` (numpy.array) - previous precision parameters 
          of normal inverse Wishart
      `psi_niw_0` (numpy.array) - previous scale parameters of inverse Wishart
      `nu_niw_0` (numpy.array) - previous df parameters of inverse Wishart
      `nb_data` (int) - sample size
      `dim_data` (int) - data dimension
      `k_trunc` (int) - truncation values of nb clusters
      
    :Returns:
        Updated parameters m_niw, psi_niw, lambda_niw, nu_niw
      
    """    
    weighted_mean = compute_weighted_mean(q_z, data, nk, 
                                        nb_data, dim_data, k_trunc)
    weighted_cov = compute_weighted_cov(q_z, data, weighted_mean, 
                                        nb_data, dim_data, k_trunc)
    
    m_niw, psi_niw, lambda_niw, nu_niw = compute_niw(nk, weighted_mean,
                                                     weighted_cov,
                                                     m_niw_0, lambda_niw_0,
                                                     psi_niw_0, nu_niw_0,
                                                     dim_data, k_trunc)
    
    return m_niw, psi_niw, lambda_niw, nu_niw


# Compute q_Z
@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def compute_exp_inv_psi(data, m_niw, lambda_niw, psi_niw, nu_niw, 
                        nb_data, dim_data, k_trunc):
    """
    Compute quadratic part q of expectation in Gaussian distributions with
    normal-inverse-Wishart-type parametrization 
    
    :Parameters:
      `data` (numpy.array) - data
      `m_niw` (numpy.array) - location parameters of normal inverse Wishart
      `lambda_niw` (numpy.array) - normal concentration parameters of 
          normal inverse Wishart
      `psi_niw` (numpy.array) - scale parameters of inverse Wishart
      `nu_niw` (numpy.array) - df parameters of inverse Wishart
      `nb_data` (int) - sample size
      `dim_data` (int) - data dimension
      `k_trunc` (int) - truncation values of nb clusters
      
    :Returns:
        [nb_data, k_trunc] np.array
        
    :Remark:
        - Includes dim_data / lamda_niw[k] + nu_niw[k] * e 
        - Does not include * -0.5
      
    """
    inv_psi = np.zeros((nb_data, k_trunc), dtype=np.float64)
    for k in range(k_trunc):
        A = dim_data / lambda_niw[k]
        psi_niw_inv = np.linalg.inv(psi_niw[k])
        for n in range(nb_data):
            iw = 0.0
            for d1 in range(dim_data):
                for d2 in range(dim_data):
                    iw += (data[n, d1] - m_niw[k, d1]) \
                        * psi_niw_inv[d1, d2] \
                        * (data[n, d2] - m_niw[k, d2])
            inv_psi[n, k] = A + nu_niw[k] * iw

    return inv_psi


@nb.jit(nopython=False, nogil=True, fastmath=True, parallel=True)
def expectation_log_gauss(data, m_niw, lambda_niw, psi_niw, nu_niw, 
                        nb_data, dim_data, k_trunc):
    """
    Compute -0.5 * expectation of log covariance determinant 
    for Gaussian distributions under variational theta* distribution.
    Constant terms wrt states z may be omitted.
    
    :Parameters:
      `data` (numpy.array) - data
      `m_niw` (numpy.array) - location parameters of normal inverse Wishart
      `lambda_niw` (numpy.array) - normal concentration parameters of 
          normal inverse Wishart
      `psi_niw` (numpy.array) - scale parameters of inverse Wishart
      `nu_niw` (numpy.array) - df parameters of inverse Wishart
      `nb_data` (int) - sample size
      `dim_data` (int) - data dimension
      `k_trunc` (int) - truncation values of nb clusters
      
    :Returns:
        [nb_data, k_trunc] np.array
    
    """

    inv_psi = compute_exp_inv_psi(data, m_niw, lambda_niw, psi_niw, nu_niw, 
                                    nb_data, dim_data, k_trunc)
    
    log_cov = np.zeros((1, k_trunc), dtype=np.float64)
    for k in range(k_trunc):   
        det_psi = max(np.linalg.det(psi_niw[k]*0.5), 1e-300)
        ld = math.log(det_psi)
        aux = 0.0
        for d in range(dim_data):
            aux += special.digamma((nu_niw[k]-d)*0.5)
        log_cov[0, k] = ld - aux

    log_gauss = log_cov + inv_psi

    return -0.5*log_gauss

@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def normalize_exp_qz(q_z, nb_data, k_trunc):
    """
    Normalization of mean-field approximation of joint distribution 
    for joint variational q_z
    """
    exp_q_z_norm = np.zeros((nb_data, k_trunc), dtype=np.float64)
    for n in range(nb_data):
        exp_q_z_norm[n, :] = q_z[n, :] - np.max(q_z[n, :])
        sum_aux = 0.0
        for k in range(k_trunc):
            sum_aux += math.exp(exp_q_z_norm[n, k])
        for k in range(k_trunc):
            exp_q_z_norm[n, k] = math.exp(exp_q_z_norm[n, k]) / sum_aux
        
    return exp_q_z_norm


@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def normalize_exp_qzj(q_zj, k_trunc):
    """
    Normalization of variational q_z in VE-z step
    """
    
    exp_q_zj_norm = q_zj - np.max(q_zj)
    sum_aux = 0.0
    for k in range(k_trunc):
        sum_aux += math.exp(exp_q_zj_norm[k])
    for k in range(k_trunc):
        exp_q_zj_norm[k] = math.exp(exp_q_zj_norm[k]) / sum_aux
        
    return exp_q_zj_norm

'''
@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def compute_local_energy(q_z, n, k_trunc, beta_z_0, neighborhood):
    
    local_energy = np.zeros(k_trunc, dtype=np.float64)
    if beta_z_0 > 0.0:
        local_list = neighborhood[n, :]
        neighbors = local_list[local_list > -1]
        for k in range(k_trunc):    
            for n in range(len(neighbors)):
                local_energy[k] += q_z[neighbors[n], k]

    return local_energy * beta_z_0


@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def compute_log_one_minus_tau(log_one_minus_tau, k_trunc):
    
    log_one_minus_tau_sum = np.zeros(k_trunc, dtype=np.float64)
    for k in range(k_trunc):
        for l in range(k):
            log_one_minus_tau_sum[k] += log_one_minus_tau[l]

    return log_one_minus_tau_sum
'''

@nb.jit(nogil=True, fastmath=True)
def expectation_z(q_z, beta_z_0, log_exp, log_tau, log_one_minus_tau, 
                    nb_data, k_trunc, neighborhood):
    """
    VE-z step 
    Compute variational q_z distribution
    
    :Parameters:
      `q_z` ([nb_data, k_trunc] np.array) - value of q_z at previous iteration
      `beta_z_0` (float) - Current value of beta
      `log_exp` ([1, k_trunc] np.array) - Expectations of log emission 
          probabilities (up to constants)
      `log_tau` ([1, k_trunc-1] np.array) - Expectations of log tau
      `log_one_minus_tau` ([1, k_trunc-1] np.array) - Expectations of log 1-tau
      `nb_data` (int) - Sample size
      `k_trunc` (int) - Truncation values of nb clusters
      `neighborhood` (np.array) - matrix of neighbors 
          (-1 means no further neighbor)
      
    :Returns:
        Variational (marginal) q_z approximation ([nb_data, k_trunc] np.array)
        
    :Remark:
        The updated q_z (q_z_new) do not come only from fixed former q_z but 
            also from already updated q_z_new
    """    
    q_z_new = np.copy(q_z)
    
    # Extends log taus to k_trunc
    log_tau_k_trunc = np.zeros(k_trunc, dtype=np.float64)
    log_tau_k_trunc[0:(k_trunc-1)] = log_tau
    
    # Sum of E[log (1-tau_l)]
    log_one_minus_tau_aux = np.zeros(k_trunc, dtype=np.float64)
    for k in range(k_trunc):
        log_one_minus_tau_aux[k] = np.sum(log_one_minus_tau[0:k])

    for n in range(nb_data):
        
        local_energy = np.zeros(k_trunc, dtype=np.float64)
        
        if beta_z_0 != 0.0:
            local_list = neighborhood[n, :]
            neighbors = local_list[local_list > -1]
            tmp = np.sum(q_z_new[neighbors, :], axis=0)
            local_energy = beta_z_0 * tmp
            
        q_z_new[n, :] = local_energy + log_exp[n, :] + log_tau_k_trunc + \
            log_one_minus_tau_aux
                    
        # normalization of q_zj:
        q_z_new[n, :] = normalize_exp_qzj(q_z_new[n, :], k_trunc)

    return q_z_new

@nb.jit(nopython=False, nogil=True, parallel=True)
def compute_log_taus(gamma_sb):
    """
    DP / PYP Prior with gamma prior on alpha 
    (stick-breaking representation with beta-distributed tau's)
    Compute E[log(tau)], E[log(1-tau)]
    """
        
    second_term = special.digamma(np.sum(gamma_sb, axis=1))
    # Compute E[log(tau)]
    log_tau = special.digamma(gamma_sb.T[0]) - second_term
    # Compute E[log(1-tau)]
    log_one_minus_tau = special.digamma(gamma_sb.T[1]) - second_term
    
    return [log_tau, log_one_minus_tau]

###############################################################################
#                        Functions for maximization steps
###############################################################################
# Update the hyperparameters gamma_k
@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def update_gamma_sb(nk, mean_alpha, mean_sigma, k_trunc):
    """
    VE-tau step 
    DP / PYP Prior with gamma prior on alpha 
    (stick-breaking representation with beta-distributed tau's)
    Update variational parameters gamma of tau posteriors

    :Parameters:
      `nk` ([1,k_trunc] np.array) - Sums of variational posteriors 
          of cluster variables z
      `mean_alpha` (float) - Expectation of variational posterior on alpha
      `mean_sigma` (float) - Expectation of variational posterior on sigma
      `k_trunc` (int) - truncation values of nb clusters
      
    :Returns:
        Variational parameters gamma of tau posteriors ([k, 2] np.array)
    """
    gamma_sb = np.zeros((k_trunc-1, 2), dtype=np.float64)
    for k in range(k_trunc-1):
        sum_aux = 0.0
        for l in range(k+1, k_trunc):
            sum_aux += nk[l]
        gamma_sb[k, 0] = 1.0 - mean_sigma + nk[k]
        gamma_sb[k, 1] = mean_alpha + (k+1)*mean_sigma + sum_aux

    return gamma_sb


def compute_dp_means(log_tau, log_one_minus_tau, s1_dp, s2_dp, k_trunc):
    """
    Expectation of alpha parameter in DPs
    """    
    mean_alpha = 0.0
    mean_sigma = 0.0
    if s1_dp > 0.0 and s2_dp > 0.0:
        mean_alpha = s1_dp/s2_dp
    else:
        mean_alpha = 0.0

    debug = False
    if debug:
        err_alpha = abs(mean_alpha - 
                       float(s1_dp) / 
                       float(s2_dp))
        msg = "abs(mean_alpha - self.process.alpha[0]/"
        msg += "self.process.alpha[1]) = " + str(err_alpha)
        print("Err_alpha: ", err_alpha)
        
    return mean_alpha, mean_sigma


# Importance sampling
def py_samples(log_tau, log_one_minus_tau, s1_py, s2_py, k_trunc, nb_samples):
    """
    In PYP NP priors, estimate expectations of PY variational parameters 
    alpha and sigma by importance sampling. 
    Return samples and weights.
    """
    ksi = 0.0
    for k in range(k_trunc-1):
        ksi += log_tau[k] - k*log_one_minus_tau[k]
    
    # Generate samples for alpha
    scale = 1.0/s2_py
    alpha_samples = stats.gamma.rvs(s1_py, scale=scale, size=nb_samples)
    
    # Generate samples for sigma
    sigma_samples = stats.uniform.rvs(0.0, 1.0, size=nb_samples)

    alpha_samples = alpha_samples - sigma_samples

    weights = np.array([], dtype=np.float64) 
    for l in range(nb_samples):
        arg1 = 1.0 - sigma_samples[l]
        arg2 = alpha_samples[l]+(k_trunc-1)*sigma_samples[l]
        fact = special.gamma(alpha_samples[l]) \
            / (special.gamma(arg2)*special.gamma(arg1)**(k_trunc-1))
        aux = 1.0
        for k in range(k_trunc-1):
            tot = alpha_samples[l] + sigma_samples[l]
            aux *= (alpha_samples[l] + k*sigma_samples[l])/tot
        arg3 = max(ksi*sigma_samples[l], -700.0)
        weights = np.append(weights, aux*fact*math.exp(-arg3))

    weights = weights/np.sum(weights)

    return alpha_samples, sigma_samples, weights


@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def compute_py_means(alpha_samples, sigma_samples, weights):
    """
    In PYP NP priors, estimate expectations of PY variational parameters 
    alpha and sigma by importance sampling. 
    Return estimates.
    """
    mean_alpha = 0.0
    mean_sigma = 0.0
    mean_log_aps = 0.0
    for l in range(len(weights)):
        mean_alpha += weights[l]*alpha_samples[l]
        mean_sigma += weights[l]*sigma_samples[l]
        mean_log_aps += weights[l]*math.log(alpha_samples[l]+sigma_samples[l])
    
    return mean_alpha, mean_sigma, mean_log_aps


@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def corr_alpha_sigma(mean_alpha, mean_sigma, alpha_samples, sigma_samples, 
                    weights):
    """
    In PYP NP priors, estimate expectations of PY variational parameters 
    alpha and sigma by importance sampling. 
    Return sample correlation between alpha and sigma.
    """
    corr_as = 0.0
    m_a2 = 0.0
    m_s2 = 0.0
    m_as = 0.0
    for l in range(len(weights)):
        m_a2 += weights[l]*alpha_samples[l]*alpha_samples[l]
        m_s2 += weights[l]*sigma_samples[l]*sigma_samples[l]
        m_as += weights[l]*alpha_samples[l]*sigma_samples[l]

    corr_as = (m_as-mean_alpha*mean_sigma) \
            / np.sqrt((m_a2-mean_alpha**2)*(m_s2-mean_sigma**2))
    
    if corr_as < -1.0:
        corr_as = -1.0
    if corr_as > 1.0:
        corr_as = 1.0

    return corr_as


@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def update_s_dp(log_one_minus_tau, s1, s2, k_trunc):
    """
    VE-alpha step 
    DP / PYP with gamma prior on alpha    
    Update hyperparameters s1 and s2
    """
    
    hat_s1 = s1 + k_trunc - 1.0
    aux = 0.0
    for l in range(k_trunc-1):
        aux += log_one_minus_tau[l]
    hat_s2 = s2 - aux

    return hat_s1, hat_s2

def gradient_s_dp(s1, k_trunc):
    """
    VE-alpha step by cancelling gradient
    """
    
    v = special.polygamma(1, s1) - 1. / s1
    
    return v

def init_update_s_dp_brent(k_trunc, maxiter=300):
    """
    Initialize optimize.brentq in cancelling s1 gradient    
    """
    min_s1 = 1.
    max_s1 = 2.

    nb_iter = 0
    # Check that gradient_s_dp change signs between min_s1 and max_s1
    iterate =  gradient_s_dp(min_s1, k_trunc) * gradient_s_dp(max_s1, k_trunc)
    while (nb_iter < maxiter) and iterate > 0:
        min_s1 = min_s1 / 2
        max_s1 = max_s1 * 2
        iterate =  gradient_s_dp(min_s1, k_trunc) * gradient_s_dp(max_s1, k_trunc)
        nb_iter += 1
        
    return min_s1, max_s1

def update_s_dp_brent(log_one_minus_tau, s1, s2, k_trunc):
    """
    VE-alpha step 
    DP / PYP with gamma prior on alpha    
    Update hyperparameters s1 and s2 using optimize.brentq
    """
    min_s1, max_s1 = init_update_s_dp_brent(k_trunc)
    if min_s1 > max_s1:
        tmp_val = max_s1
        max_s1 = min_s1
        min_s1 = tmp_val
    min_s1 = max(min_s1, np.finfo(np.float64).tiny)
    
    debug = True
    if debug:
        min_val = gradient_s_dp(min_s1, k_trunc)
        max_val = gradient_s_dp(max_s1, k_trunc)
        if min_val > max_val:
            tmp_val = max_val
            max_val = min_val
            min_val = tmp_val
        print("Initial s gradient values: " + str((min_val, max_val)))
    try:
        s1_ast = optimize.brentq(gradient_s_dp, min_s1, max_s1, 
                                 args=(k_trunc), 
                                 maxiter=30000, full_output=False)
        math.log(s1_ast)
    except (ValueError, RuntimeError):
        print("Warning: No solution for VE-alpha step.")
        hat_s1 = s1
        hat_s2 = s2
    else:
        hat_s1 = s1_ast        
        hat_s2 = -hat_s1 * log_one_minus_tau[0:(k_trunc-1)].sum() / (k_trunc-1)
    
    if debug:
        msg = "Re-estimating (s1,s2):" + str((hat_s1, hat_s2))
        if hat_s2 <= 0:
            msg += "log_one_minus_tau[0:-1].sum(): " + \
                str(log_one_minus_tau[0:-1].sum()) + "\n"
            msg += "1./(k_trunc-1): " + str(1./(k_trunc-1))
        assert(hat_s1 > 0 and hat_s2 > 0), msg
        
    return hat_s1, hat_s2

@nb.jit(nopython=False, nogil=True, fastmath=True, parallel=False)
def compute_tau_tilde(gamma_sb, k_trunc):
    """
    Compute mean taus
    
    :Returns:
        
    """
   
    tau_tilde = np.zeros(k_trunc, dtype=np.float64)
    
    for k in range(k_trunc-1):
        tau_tilde[k] = gamma_sb[k, 0] / (gamma_sb[k, 0] + gamma_sb[k, 1])
    
    tau_tilde[k_trunc-1] = 1.

    log_pi_tilde = compute_log_pi_tilde(tau_tilde, k_trunc)
    
    return tau_tilde, log_pi_tilde

@nb.jit(nopython=False, nogil=True, fastmath=True, parallel=False)
def compute_log_pi_tilde(taus, k_trunc):
    """
    Compute log \pi(\tau) from tau values (means or random sample)
    
    :Returns:
        
    """

    sum_log_one_minus_tau = 0.

    log_one_minus_tau = np.zeros(k_trunc, dtype=np.float64)    
    log_pi_tilde = np.zeros(k_trunc, dtype=np.float64)
    
    for k in range(1,k_trunc):
        log_one_minus_tau[k-1] = math.log(1-taus[k-1])
        log_pi_tilde[k-1] += math.log(taus[k-1])
        sum_log_one_minus_tau += log_one_minus_tau[k-1]
        log_pi_tilde[k] += sum_log_one_minus_tau
    
    # Not used
    # log_one_minus_tau[k_trunc-1] = math.log(1-taus[k_trunc-1])
       
    return log_pi_tilde

@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=True)
# @nb.jit(nopython=False, nogil=True, fastmath=True, parallel=True)
def compute_q_z_tilde(beta_z, q_z, gamma_sb, nb_data, k_trunc, nb_contrib):
    """
    Mean-field (log, unnormalized) approximation of joint distribution 
    for joint variational q_z
    
    :Returns:
        Contributions of log pi + pairs * beta to mean-field (unnormalized) 
        approximation of joint distribution for joint variational q_z 
        ([nb_data, k_trunc] np.array)
    """

    tau_tilde, _ = compute_tau_tilde(gamma_sb, k_trunc)
    log_pi_tilde = compute_log_pi_tilde(tau_tilde, k_trunc)
    
    return compute_q_z_tilde_log_pi(beta_z, q_z, log_pi_tilde, 
                                    nb_data, k_trunc, nb_contrib)

@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=True)
# @nb.jit(nopython=False, nogil=True, fastmath=True, parallel=True)
def compute_q_z_tilde_log_pi(beta_z, q_z, log_pi, nb_data, k_trunc, nb_contrib):
    """
    Mean-field (log, unnormalized) approximation of joint distribution 
    for joint variational q_z using given values for log_pi 
    
    :Returns:
        Contributions of log pi + pairs * beta to mean-field (unnormalized) 
        approximation of joint distribution for joint variational q_z 
        ([nb_data, k_trunc] np.array)
    """
    
    q_z_tilde = np.zeros((nb_data, k_trunc), dtype=np.float64)
    log_mrf = pairwise_energy(beta_z, nb_contrib)
    
    for n in range(nb_data):
        for k in range(k_trunc):
            q_z_tilde[n, k] = log_pi[k] + log_mrf[n, k]

    return q_z_tilde

# @nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
@nb.jit(nopython=False, nogil=True, fastmath=True, parallel=False)
def compute_q_z_tilde_tau(beta_z, q_z, taus, nb_data, k_trunc, nb_contrib):
    """
    Mean-field (log, unnormalized) approximation of joint distribution 
    for joint variational q_z using given values for taus (e.g., MC-simulated)
    
    :Returns:
        Contributions of log pi + pairs * beta to mean-field (unnormalized) 
        approximation of joint distribution for joint variational q_z 
        ([nb_data, k_trunc] np.array)
    """

    log_pi = compute_log_pi_tilde(taus, k_trunc)
            
    return compute_q_z_tilde_log_pi(beta_z, q_z, log_pi, nb_data, 
                                    k_trunc, nb_contrib)

@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def gradient_beta(beta_z, q_z, gamma_sb, nb_data, k_trunc, neighborhood,
                  nb_contrib):
    """
    M-beta step with mean-field like approximation: gradient computation
    
    Compute approximated gradient of expected energy function - expected 
    normalizing constant
    """
    # Mean field approx.
    q_z_tilde = compute_q_z_tilde(beta_z, q_z, gamma_sb, nb_data, k_trunc, 
                                  nb_contrib)
    
    # Mormalize q_z_tilde:
    q_z_tilde = normalize_exp_qz(q_z_tilde, nb_data, k_trunc)

    grad_interaction = np.multiply(nb_contrib, q_z).sum()
    grad_normalization = 0.0
    
    for n in range(nb_data):
        local_list = neighborhood[n, :]
        neighbors = local_list[local_list > -1]
        for k in range(k_trunc):
            aux = 0.0
            for i in range(len(neighbors)):
                aux += q_z_tilde[neighbors[i], k]
            grad_normalization += aux * q_z_tilde[n, k]

    return (grad_interaction - grad_normalization) * 0.5


@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def gradient_beta_concave(beta_z, q_z, gamma_sb, nb_data, k_trunc, 
                          neighborhood, nb_contrib):
    """
    M-beta step with mean-field like approximation and concave approximation
    of gradient: computation
    
    Compute approximated gradient of expected energy function - expected 
    normalizing constant
    
    :Remark:
        `neighborhood` is not actually used but kept for interchangeability
        of generic gradient functions
    """
    # Mean field approx.
    q_z_tilde = compute_q_z_tilde(beta_z, q_z, gamma_sb, nb_data, k_trunc, 
                                  nb_contrib)
    
    # Mormalize q_z_tilde:
    q_z_tilde = normalize_exp_qz(q_z_tilde, nb_data, k_trunc)

    grad_interaction = np.multiply(nb_contrib, q_z).sum()
    grad_normalization = np.multiply(nb_contrib, q_z_tilde).sum()
    
    return 0.5 * (grad_interaction - grad_normalization)

@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
# Strangely, seems more efficient than parallel=True
def gradient_beta_mc_generic(beta_z, q_z, gamma_sb, nb_data, k_trunc, 
                             neighborhood, nb_contrib, sim_tau):
    """
    M-beta step with mean-field like and MC approximations: 
        gradient computation
    
    Compute approximated gradient of expected energy function - expected 
    normalizing constant
    """
    # Mean field approx.
    mc_iters = sim_tau.shape[0]
    q_z_tilde = np.zeros((nb_data, k_trunc, mc_iters), dtype=np.float64)
    # for t in prange(mc_iters):
    for t in range(mc_iters):
        q_z_tilde[:,:,t] = compute_q_z_tilde_tau(beta_z, q_z, sim_tau[t,:],
                                                 nb_data, k_trunc,
                                                 nb_contrib)
        # Normalize q_z_tilde:
        q_z_tilde[:,:,t] = normalize_exp_qz(q_z_tilde[:,:,t], nb_data, k_trunc)

    grad_interaction = np.multiply(q_z, nb_contrib).sum()
    grad_normalization = 0.0
    
    for n in range(nb_data):
        local_list = neighborhood[n, :]
        neighbors = local_list[local_list > -1]
        for k in range(k_trunc):
            aux = 0.0
            for i in range(len(neighbors)):
                aux += np.dot(q_z_tilde[n, k, :], q_z_tilde[neighbors[i], k, :])
            grad_normalization += aux / mc_iters

    return (grad_interaction - grad_normalization) * 0.5

@nb.jit(nopython=False, nogil=False, fastmath=True, parallel=True)
# nopython=True does not work here for unknown reasons
# @nb.jit(nopython=True, nogil=False, fastmath=True, parallel=True)
def gradient_beta_mc_concave_generic(beta_z, q_z, gamma_sb, nb_data, k_trunc, 
                                     neighborhood, nb_contrib, sim_tau):
    """
    M-beta step with mean-field like and MC approximations: 
        gradient computation using concave approximation
    
    Compute approximated gradient of expected energy function - expected 
    normalizing constant
    """
    # Mean field approx.
    mc_iters = sim_tau.shape[0]
    q_z_tilde = np.zeros((nb_data, k_trunc, mc_iters), dtype=np.float64)
    for t in prange(mc_iters):  # prange much faster here
    # for t in range(mc_iters):
        q_z_tilde[:,:,t] = compute_q_z_tilde_tau(beta_z, q_z, sim_tau[t,:],
                                                 nb_data, k_trunc,
                                                 nb_contrib)
        # Normalize q_z_tilde:
        q_z_tilde[:,:,t] = normalize_exp_qz(q_z_tilde[:,:,t], nb_data, k_trunc)

    q_z_tilde_m = q_z_tilde.mean(axis=2)
    grad_interaction = np.multiply(nb_contrib, q_z).sum()
    grad_normalization = np.multiply(nb_contrib, q_z_tilde_m).sum()

    return 0.5 * (grad_interaction - grad_normalization)

"""
@nb.jit(nopython=False, nogil=False, fastmath=True, parallel=True)
def gradient_beta_mc_concave_generic(beta_z, q_z, gamma_sb, nb_data, k_trunc, 
                                     neighborhood, sim_tau):

    # Mean field approx.
    mc_iters = sim_tau.shape[0]
    q_z_tilde = np.zeros((nb_data, k_trunc, mc_iters), dtype=np.float64)
    for t in range(mc_iters):
        q_z_tilde[:,:,t] = compute_q_z_tilde_tau(beta_z, q_z, sim_tau[t,:],
                                                 nb_data, k_trunc, neighborhood)
        # Normalize q_z_tilde:
        q_z_tilde[:,:,t] = normalize_exp_qz(q_z_tilde[:,:,t], nb_data, k_trunc)

    grad_interaction = 0.0
    grad_normalization = 0.0
    
    for n in range(nb_data):
        local_list = neighborhood[n, :]
        neighbors = local_list[local_list > -1]
        for k in range(k_trunc):
            aux1 = 0.0
            for i in range(len(neighbors)):
                aux1 += q_z[neighbors[i], k] 
            grad_interaction += aux1 * q_z[n, k]
            grad_normalization += aux1 * q_z_tilde[n, k, :].mean() / mc_iters

    return (0.5 * grad_interaction - grad_normalization)
"""

@nb.jit(nopython=True, nogil=True, parallel=True)
def part1_gradient_beta(q_z, gamma_sb, nb_data, k_trunc, neighborhood, 
                        nb_contrib):
    """
    Compute expected gradient of free energy (pair interactions)
    """
    grad_interaction = np.multiply(q_z, neighborhood).sum()

    return grad_interaction 


@nb.jit(nopython=True, nogil=True, parallel=True)
def part2_gradient_beta(beta_z, q_z, gamma_sb, nb_data, k_trunc, neighborhood):
    """
    Compute gradient of expected log normalization constant
    """
    q_z_tilde = compute_q_z_tilde(beta_z, q_z, gamma_sb, 
                                nb_data, k_trunc, neighborhood)
    
    # Mormalize q_z_tilde:
    q_z_tilde = normalize_exp_qz(q_z_tilde, nb_data, k_trunc)

    grad_normalization = np.float64(0.0)
    
    for n in nb.prange(nb_data):
        local_list = neighborhood[n, :]
        neighbors = local_list[local_list > -1]
        for k in nb.prange(k_trunc):
            aux2 = np.float64(0.0)
            for i in nb.prange(len(neighbors)):
                aux2 += q_z_tilde[neighbors[i], k]
            grad_normalization += aux2 * q_z_tilde[n, k]

    return grad_normalization


def gradient_s1(s1, mean_alpha, mean_sigma, mean_log_aps):
    
    f = math.log(s1) - special.digamma(s1) \
        + mean_log_aps - math.log(mean_alpha+mean_sigma)

    return f


# Solve the equations for s1 and s2
def update_s_py(mean_alpha, mean_sigma, mean_log_aps, s1, s2):
    
    hat_s1 = 0.0
    hat_s2 = 0.0
    try:
        s1_ast = optimize.brentq(gradient_s1, 1e-10, 1e10, 
                                args=(mean_alpha, mean_sigma, mean_log_aps), 
                                maxiter=300, full_output=False)
        hat_s1 = s1_ast
    except (ValueError, RuntimeError):
        print("Warning: No solution for the M-s1 step.")
        hat_s1 = s1
    
    if mean_alpha + mean_sigma == 0.0:
        hat_s2 = s2
    else:
        hat_s2 = hat_s1 / (mean_alpha+mean_sigma)
    
    return hat_s1, hat_s2


def update_beta_z(q_z, gamma_sb, model, nb_data, 
                    k_trunc, beta_z_0, neighborhood):
    """
    M-beta step with mean-field like approximation: numerical 
    black-box optimization using gradient function
    
    :Remark:
        Deprecated
        
    :seealso:
        potts_model.update_beta_z
    """
    beta_z = 0.0
        
    if (model == 'dp-mrf') or (model == 'py-mrf'):
        try:
            beta_ast = optimize.brentq(gradient_beta, -10., 10., 
                                    args=(q_z, gamma_sb, nb_data, 
                                        k_trunc, neighborhood), 
                                        maxiter=300, full_output=False)
            beta_z = beta_ast
        except (ValueError, RuntimeError):
            print("Warning: No solution for the M-beta step.")
            beta_z = beta_z_0
    
    return beta_z

@nb.jit(nopython=True, nogil=True, parallel=True)
def discrete_entropy(p):
    """
    Compute joint entropy for a matrix of distributions p
    Each line of p defines the distribution of a random variable.
    Random variables associated with each line are assumed independent.
    
    :Parameters:
      `p` ([nb_data, nb_values] np.array) - matrix defining distributions
      
    :Returns:
        Entropy (float)   
    """
    nb_data, nb_values = p.shape    
    
    entropy = 0.
    for n in nb.prange(nb_data):
        for k in nb.prange(nb_values):
            if p[n, k] > 0:
                entropy += p[n, k] * math.log(p[n, k])

    return -entropy

@nb.jit(nopython=False, nogil=True, parallel=True)
def compute_pi_tau_entropy(beta_z, q_z, taus):
    """
    Compute contribution of log pi_z(tau) to (joint) entropy 
    of log p(z|tau, beta)

    :Parameters:
        `beta_z` (float) - beta estimate at current iteration
        `q_z` (([nb_data, k_trunc] np.array)) - variational approximation 
            of state distribution
        `taus` (np.array) - Parameters gamma (2-dimensional) of tau 
            variational posteriors
    :Returns:
        nb_data (int) - sample size
        k_trunc (int) - truncation number for clusters
        nk (np.array) - per-state sums of q_z
        Entropy (float)   
    """
    nb_data, k_trunc = q_z.shape
  
    nk = q_z.sum(axis=0)

    # E[log pi_k(tau)]

    [log_tau, log_one_minus_tau] = compute_log_taus(taus)
    
    # Extends log taus to k_trunc
    log_tau_k_trunc = np.zeros(k_trunc, dtype=np.float64)
    log_tau_k_trunc[0:(k_trunc-1)] = log_tau
    
    # log_one_minus_tau_aux[k] = \sum_{l=0}^{k-1} E[log (1-tau_l)]
    log_one_minus_tau_aux = np.zeros(k_trunc, dtype=np.float64)
        
    entropy = nk[0] * log_tau[0]
    
    for k in range(1, k_trunc):
        log_one_minus_tau_aux[k] = log_one_minus_tau_aux[k-1] + \
            log_one_minus_tau[k-1]
        log_tau_k_trunc[k] += log_one_minus_tau_aux[k]
        entropy += nk[k] * log_tau_k_trunc[k]
        
    return nb_data, k_trunc, nk, entropy
  
@nb.jit(nopython=False, nogil=True, parallel=True)
def compute_z_tau_entropy(beta_z, q_z, taus, neighborhood, nb_contrib):
    """
    Compute (joint) entropy of log p(z|tau, beta)

    :Parameters:
        `beta_z` (float) - beta estimate at current iteration
        `q_z` (([nb_data, k_trunc] np.array)) - variational approximation 
            of state distribution
        `taus` (np.array) - Parameters gamma (2-dimensional) of tau 
            variational posteriors
        `neighborhood` (np.array) - matrix of neighbors 
              (-1 means no further neighbor)
        `nb_contrib` (np.array) - matrix of contributions of neighbors 
              to free energy

    :Returns:
        Entropy (float)   
    """   
    
    nb_data, k_trunc, nk, entropy = compute_pi_tau_entropy(beta_z, q_z, taus)
    
    """log_one_minus_tau_aux[k_trunc-1] = log_one_minus_tau_aux[k_trunc-2] + \
            log_one_minus_tau[k_trunc-1]
    log_tau_k_trunc[k_trunc-1] = log_one_minus_tau_aux[k_trunc-1]
    entropy += nk[k_trunc-1] * log_tau_k_trunc[k_trunc-1]"""
    
    interaction = np.multiply(nb_contrib, q_z).sum()
    
    entropy += beta_z * 0.5 * interaction

    # Entropy for log normalization constant
    q_z_tilde = compute_q_z_tilde(beta_z, q_z, taus, nb_data, 
                                   k_trunc, nb_contrib)
    # exp_q_z_tilde = np.exp(q_z_tilde)
    # nrm = exp_q_z_tilde.sum(1)
    # nrm = np.log(nrm)
    # nrm = nrm.sum(0)
    
    nrm = 0.
    nrm2 = 0.
    
    # Compute mean-field approx.:

    # E[tau]
    mean_taus = taus[:,0] / taus.sum(axis=1)
    mean_pi = np.zeros(k_trunc, dtype=np.float64)
    log_prod_one_minus_tau = np.zeros(k_trunc, dtype=np.float64)
    mean_pi[0] = mean_taus[0]
    log_prod_one_minus_tau[0] = math.log(1-mean_taus[0])
    
    for k in range(1, k_trunc-1):
        mean_pi[k] = math.exp(log_prod_one_minus_tau[k-1]) * mean_taus[k]
        log_prod_one_minus_tau[k] = log_prod_one_minus_tau[k-1] + \
            math.log(1-mean_taus[k])
    
    mean_pi[k_trunc-1] = 1.
    
    # Normalize q_z_tilde
    q_z_tilde = normalize_exp_qz(q_z_tilde, nb_data, k_trunc)
    
    for j in range(nb_data):
        local_list = neighborhood[j, :]
        neighbors = local_list[local_list > -1]
        aux_pi = 0.
        for k in range(k_trunc):
            # contributions of neighbors
            aux_mf = 0.# MF
            aux_q_z = 0. # q_z
            for i in range(len(neighbors)):
                aux_mf += q_z_tilde[neighbors[i], k]
                aux_q_z += q_z[neighbors[i], k]                
            nrm2 += q_z_tilde[j,k] * (0.5 * aux_mf - aux_q_z)
            aux_pi += mean_pi[k] * math.exp(beta_z * aux_q_z)
        nrm += math.log(aux_pi)

    entropy = entropy - nrm - beta_z * nrm2
    
    return(entropy)


###############################################################################
#      Pairwise probability matrix for finding optimal clustering (attempts)
###############################################################################
# Memory problem since the matrix dimensions are too huge to handle
@nb.jit(nopython=True, nogil=True, parallel=True)
def estimate_psm(q_z, nb_data, k_trunc):

    psmat = np.zeros((nb_data, nb_data), dtype=np.float64)
    for i in nb.prange(nb_data):
        for j in nb.prange(nb_data):
            if i == j:
                psmat[i, j] = 1.0
            else:
                for k in nb.prange(k_trunc):
                    psmat[i, j] += q_z[i, k] * q_z[j, k]
    
    return psmat


# Memory problem since the matrix dimensions are too huge to handle
@nb.jit(nopython=True, nogil=True, parallel=True)
def indicator_mu(m_niw, k1, k2, threshold):
    diff = 0.0
    for d in nb.prange(m_niw.shape[1]):
        diff += (m_niw[k1, d] - m_niw[k2, d]) ** 2
    if np.sqrt(diff) < threshold:
        return 1.0
    else:
        return 0.0


@nb.jit(nopython=True, nogil=True, parallel=True)
def estimate_psm_with_mu(q_z, m_niw, nb_data, k_trunc, threshold):

    psmat = np.zeros((nb_data, nb_data), dtype=np.float64)
    for i in nb.prange(nb_data):
        for j in nb.prange(nb_data):
            if i == j:
                psmat[i, j] = 1.0
            else:
                for k1 in nb.prange(k_trunc):
                    for k2 in nb.prange(k1+1, k_trunc):
                        if indicator_mu(m_niw, k1, k2, threshold) == 1.0:
                            psmat[i, j] += q_z[i, k1] * q_z[j, k2]
    
    return psmat

###############################################################################
#               Label initialization with Kmeans++
###############################################################################
'''
@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def init_lambda(data, q_z, nk, nb_data, dim_data, k_trunc):
    
    weighted_mean = compute_weighted_mean(q_z, data, nk, 
                                        nb_data, dim_data, k_trunc)
    weighted_cov = compute_weighted_cov(q_z, data, weighted_mean, 
                                        nb_data, dim_data, k_trunc)
    
    lambda_niw_0 = np.ones(k_trunc, dtype=np.float64)
    for k in range(k_trunc):
        # safe division:
        if nk[k] > 0.0:
            lambda_niw_0[k] = np.trace(weighted_cov[k]) / nk[k] / dim_data

    return lambda_niw_0
'''

@nb.jit(nopython=True, nogil=True, fastmath=True, parallel=False)
def init_q_z(labels, nb_data, k_trunc):
    
    q_z = np.zeros((nb_data, k_trunc), dtype=np.float64)
    for i in range(nb_data):
        # q_z[i, np.random.choice(k_trunc, 1)] = 1.0
        q_z[i, labels[i]] = 1.0
    
    return q_z


def init_by_kmeans(data, nb_data, dim_data, k_trunc, nb_init, seed=100, 
                   init_labels=None):
    """
    Initialize variational q_z by k-means and update normal inverse-Wishart
    parameters
    """
    m_niw_0 = np.zeros((k_trunc, dim_data), dtype=np.float64)
    psi_niw_0 = np.zeros((k_trunc, dim_data, dim_data), dtype=np.float64)
    psi_niw_0[:] = np.eye(dim_data, dtype=np.float64) * 1000.0
    nu_niw_0 = np.ones(k_trunc, dtype=np.float64) * dim_data
    lambda_niw_0 = np.ones(k_trunc, dtype=np.float64)
    
    if init_labels is None:
        import cv2
        # convert to np.float32
        arr = np.float32(data)
        # Define criteria
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.1)
        # Use kmeans++ center initialization by Arthur 2007
        flags = cv2.KMEANS_PP_CENTERS
        # flags = cv2.KMEANS_RANDOM_CENTERS
        cv2.setRNGSeed(seed)
            
        _, labels, _ = cv2.kmeans(data=arr, K=k_trunc, bestLabels=None, 
                                criteria=criteria, attempts=nb_init, flags=flags)
    
        labels = labels.T[0]
    else:
        labels = init_labels
        
    q_z = init_q_z(labels, nb_data, k_trunc)
    '''
    from sklearn.mixture import BayesianGaussianMixture    
    
    bgmm = BayesianGaussianMixture(n_components=k_trunc, init_params="kmeans", 
                                    random_state=0, n_init=nb_init).fit(data)
    
    q_z = bgmm.predict_proba(data)
    '''

    nk = q_z.sum(axis=0) + 10 * np.finfo(q_z.dtype).eps
    
    return q_z, nk, labels


