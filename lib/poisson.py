import numpy as np
import numba as nb
import math
import scipy.special as special
from distribution import Distribution
from vbem_utils_numba import normalize_exp_qz, init_q_z, discrete_entropy, \
    update_gamma_sb, compute_pi_tau_entropy, init_by_kmeans
from preprocessing import load_labels

import warnings
warnings.filterwarnings("ignore")


class Poisson(Distribution):
    """ 
    Collection of Poisson emission distributions.
    
    self.parameter corresponds to posterior means of parameters theta, 
        whose first axis is k_trunc
    self.prior corresponds to the prior parameters (updated in M step)
    
    :Remark: 
        - methods using data have their data arguments organized in two columns:
            1/ random counts y_j; 
            2/ fixed size n_j of total population (disease mapping models)
        - prior is Gamma parametered by shape a and rate b ($l^{a-1} e^{-bl}$)
            
    """

    
    def __init__ (self, var_name, nbinit, dim=1, init_method="k_means_only", 
                 init_file=""):
        """
        :Parameters:
            `var_name` (str) - name of variable in data file? (if any...)
            `nbinit` (int) - number of random initializations
            `dim` (int) -  data dimension
            `init_method` (str) - method used for initialization
                ("k_means_only" or "free_energy" - 10 runs of k-means then keep
                highest free energy)                    
            `init_file` (str) - path to file containing initial labels if 
                init_method is "file"                
        """
        # Set nbinit
        super(Poisson, self).__init__(nbinit)
        
        self.dist_name = "poisson"
        self.var_name = var_name
        self.parameter = []
        self.prior = {"a": 0.0, "b": 0.0}
        self.dim = dim
        self.cur_mle = -1e100
        self.init_method = init_method
        self.init_file = init_file
        

    def __str__(self):
        return "variable " + self.var_name + ": " + self.dist_name +" distribution" \
                + "\nparameters: " + str(self.parameter) \
                + "\n prior parameters: " + str(self.prior) \
                + "\n posterior distibution parameters: " + str(self.post_param) \
                + "\n number of initializations: " + str(self.nb_init) \
                + "\n initialization method: " + str(self.init_method) 
                
    def copy(self):
        """
        Copy current model
        """
        p = Poisson(self.var_name, self.dim)
        p.dist_name = self.dist_name        
        p.var_name = self.var_name        
        p.parameter = np.copy(self.parameter)
        p.prior = dict(self.prior)
        p.cur_mle = self.cur_mle
        p.init_method = self.init_method
        p.init_file = self.init_file
        
        return p
    
    
    def init(self, data, nb_data, dim_data, k_trunc, seed, nbinit):
        """
        Initialization method
        """
        
        #------------------ init lambda_k's and prior -------------------
        """       
        # init parameters and partition together: MAP partition
        lambdas, labels = init_stra_MAP(data, nb_data, k_trunc)
        a, b  = init_a_b(lambdas, k_trunc)
        q_z = init_q_z_(data, a, b, nb_data, k_trunc)
        """
        #----------- Markov Random field parameters initialization -----------
        # init parameters and partition together: Kmeans partition
        s1 = 1.4
        s2 = 1.0
        mean_sigma = 0.0
        beta_z_0 = 0.0

        import numpy as np
        ratio = np.zeros((nb_data, 1), dtype=np.float64)
        ratio[:,0] = data[:,0].astype(np.float64) / data[:,1].astype(np.float64)
        
        if self.init_method == "k_means_only":
            a, b, lambdas, q_z = \
                k_means_init(data, ratio, nb_data, k_trunc, seed, 
                             nbinit, labels=None)
        elif self.init_method == "free_energy":
            # initial q_z
             q_z, s1, s2, mean_sigma, beta_z_0, a, b, lambdas = \
                 fast_init_free_energy(data, nb_data, k_trunc, seed, s1, s2, 
                                       mean_sigma, beta_z_0, nbinit)
        elif self.init_method == "file":            
            labels =  load_labels(self.init_file, k_trunc)
            a, b, lambdas, q_z = \
                k_means_init(data, ratio, nb_data, k_trunc, seed, 
                             nbinit, labels=labels)
        
        #---------Set fields---------------
        self.dim = dim_data
        self.parameters = lambdas
        self.prior["a"] = a
        self.prior["b"] = b

        return q_z, s1, s2, mean_sigma, beta_z_0
        
    def update_posterior(self, data, q_z, nk, nb_data, dim_data, k_trunc):
        """
        Update hyperparameters rho
        
        a: (nb_data, dim_data) array
		data[0,:] = y_j number of observed cases within cattle j
		data[1,:] = n_j cattle population size
	     q_z: (nb_data, k_trunc) array
		q_z[j,k] = P(z_j = k)
        """

        a_ks = np.copy(self.prior["a"])
        b_ks = np.copy(self.prior["b"])
       
        hat_a, hat_b = update_gamma(data, a_ks, b_ks, q_z, nb_data, k_trunc)
        
        self.prior["a"] = hat_a
        self.prior["b"] = hat_b
        self.parameter = update_lambda(hat_a, hat_b, k_trunc)


    def expectation_likelihood(self, data, nb_data, dim_data, k_trunc):
        """
	    return (nb_data, k_trunc) array
        """
        a_ks = np.copy(self.prior["a"])
        b_ks = np.copy(self.prior["b"])
        exp = expectation_log_pois(data, a_ks, b_ks, nb_data, k_trunc)
        
        return exp

    
    def convergence(self, model, deltas, threshold):       
        
        delta_beta, delta_qz, delta_elbo = deltas
        if model == "dp" or model == "pyp":
            return delta_qz < threshold
        elif model == "dp-mrf" or model == "pyp-mrf":
            return super(Poisson, self).convergence(deltas, threshold)
        else:
            print("Warning: Please check the model name.")
            raise SystemExit

    def entropy_theta(self, data, q_z):
        """
        Contribution of variational q_theta distribution to entropy
        (up to constants wrt hyperparameters Phi and variational parameters)
        
        :Parameters:
            `data` (np.array) - data
            `q_z` (([nb_data, k_trunc] np.array)) - variational approximation 
            of state distribution    
        """
               
        return(compute_entropy_theta(data, q_z, self.prior["a"], self.prior["b"]))

#######################################################################################
#			 utils functions
#######################################################################################
           
# numbda does not support np.random.dirichlet
def init_s_tra(data, nb_data, k_trunc, alpha):
  
    # y_i/n_i	
    samples = np.array(data[:,0]/data[:,1], dtype=np.float64)
    lambdas_mean = np.sum(data[:,0])/np.sum(data[:,1])
    lambda_k = -1  
    itr = 0

    while lambda_k <= 0:    
        # step 1
        nk_0 = np.random.dirichlet(alpha=alpha, size=1)[0]

        # step 2: discard null observations y_i before sampling 
        lambdas_0 = np.random.choice(a=samples[samples != 0], size=k_trunc, replace=False)

        k = np.random.randint(0, k_trunc, 1)[0]
        aux_sum = 0.0

        for l in range(k_trunc):
            if l != k:
                aux_sum = aux_sum + nk_0[l] * lambdas_0[l]

        lambda_k = (lambdas_mean - aux_sum)/nk_0[k] 
        lambdas_0[k] = lambda_k
        
        itr += 1

    print("lambdas initilized after {} attempts".format(itr))

    return lambdas_0
               
    
def init_q_z_(data, a_k, b_k, nb_data, k_trunc):
    
    # MLE part of qz when beta=0, pi_k = 1/k_trunc, lambda_k's fixed 
    qz_init = expectation_log_pois(data, a_k, b_k, nb_data, k_trunc)
    qz_init = normalize_exp_qz(qz_init, nb_data, k_trunc)
    
    return qz_init


def init_stra_MAP(data, nb_data, k_trunc):

    samples = np.zeros(nb_data, dtype=np.float64)
    for i in range(nb_data):
        samples[i] = data[i,0]/data[i,1]

    labels = np.zeros(nb_data, dtype=np.int32)
    lambdas = np.zeros(nb_data, dtype=np.float64)
    max_mle = -1e100

    it = 0
    while it < 100:

        # discard null observations y_i before sampling 
        c_lambdas = np.random.choice(a=samples[samples != 0],
                                     size=k_trunc, replace=False)

        a, b  = init_a_b(c_lambdas, k_trunc)
        q_z = init_q_z_(data, a, b, nb_data, k_trunc)
        c_labels = np.argmax(q_z, axis=1)
        
        # MLE of this partition
        c_mle = 0.0
        for j in range(nb_data):
            y_j = data[j,0]
            n_j = data[j,1]
            c_mle = c_mle + y_j * math.log(max(1e-30, c_lambdas[c_labels[j]])) - \
                                            n_j * c_lambdas[c_labels[j]]
        if c_mle > max_mle:
            labels = c_labels
            lambdas = c_lambdas
            max_mle = c_mle
                                           
        it = it + 1
        
    print("init_risk = ", lambdas)

    return lambdas, labels

def init_stra_Kmean(data, nb_data, k_trunc, seed, nbinit):

    from sklearn.cluster import KMeans
    # Set seed for sampling
    np.random.seed(seed)
    
    samples = np.zeros(nb_data, dtype=np.float64)
    for i in range(nb_data):
        samples[i] = data[i,0]/data[i,1]

    labels = np.zeros(nb_data, dtype=np.int32)
    lambdas = np.zeros(nb_data, dtype=np.float64)
    max_mle = -1e100

    it = 0
    while it < nbinit:

        # discard null observations y_i before sampling 
        c_lambdas = np.random.choice(a=samples[samples != 0],
                                     size=k_trunc, replace=False)

        # kmean partition
        _kmeans = KMeans(n_clusters=k_trunc, max_iter=int(1e6), tol=1e-16,
                     init=c_lambdas.reshape(-1,1)).fit(samples.reshape(-1,1))
        c_labels =  _kmeans.labels_
        
        # MLE of this partition
        c_mle = 0.0
        for j in range(nb_data):
            y_j = data[j,0]
            n_j = data[j,1]
            c_mle = c_mle + y_j * math.log(max(1e-300, c_lambdas[c_labels[j]])) - \
                                            n_j * c_lambdas[c_labels[j]]
        if c_mle > max_mle:
            labels = c_labels
            lambdas = c_lambdas
            max_mle = c_mle
                                           
        it = it + 1

    return lambdas, labels

def init_a_b(lambdas, k_trunc):
    # choose parameters a and b so that mean is lambda and variance is v2.
    a = np.zeros(k_trunc, dtype=np.float64)
    b = np.zeros(k_trunc, dtype=np.float64)   
    
    v2 = np.min(lambdas[lambdas != 0])
    for k in range(k_trunc):   
  
        assert(lambdas[k] != 0)
        a[k] = lambdas[k] * lambdas[k] /v2
        b[k] = lambdas[k]  /v2

    return a, b

@nb.jit(nopython=True, parallel=True, nogil=True)
def update_gamma(data, a_gam, b_gam, q_z, nb_data, k_trunc):

    hat_a = np.zeros(k_trunc, dtype=np.float64) 
    hat_b = np.zeros(k_trunc, dtype=np.float64)

    for k in range(k_trunc):
        
        sum_a = 0.0
        sum_b = 0.0
        for j in range(nb_data):
            y_j = data[j, 0]
            n_j = data[j, 1]
            sum_a = sum_a + y_j * q_z[j, k]	
            sum_b = sum_b + n_j * q_z[j, k]

        hat_a[k] = a_gam[k] + sum_a
        hat_b[k] = b_gam[k] + sum_b

    return hat_a, hat_b


@nb.jit(nopython=True, nogil=True, parallel=True)
def update_lambda(hat_a, hat_b, k_trunc):
	
    lambdas = np.zeros(k_trunc, dtype=np.float64)	
    for k in range(k_trunc):

        if hat_b[k] == 0.0:
            lambdas[k] = 0.0
        else: 
            lambdas[k] = hat_a[k]/hat_b[k]

    return lambdas

@nb.jit(nopython=False, parallel=True, nogil=True)
def estimate_lambdas(data, q_z, nk):
    """
    Weighted ML estimation of lambdas from data, q_z and nk
    """
    min_lambda = 1e-30
    nb_data, k_trunc = q_z.shape

    lambdas = np.zeros(nb_data, dtype=np.float64)    
    
    # Non-weighted MLE: data[:,0].sum() / data[:,1].sum()
    for k in range(k_trunc):
        dn = np.dot(q_z[:,k], data[:,1])
        if dn > 0:
            lambdas[k] = max(np.dot(q_z[:,k], data[:,0]) / dn , min_lambda)
        else:
            lambdas[k] = min_lambda
    
    return lambdas
	
@nb.jit(nopython=False, parallel=True, nogil=True)
def expectation_log_pois(data, a_gam, b_gam, nb_data, k_trunc):
    """
    Expectations of log Poisson pdfs (up to state-independent terms)
    """
    
    exp = np.zeros((nb_data, k_trunc), dtype=np.float64)
    
    # Expectations of log Poisson parameters 
    exp_log_lambdas = np.zeros((k_trunc), dtype=np.float64) 
    
    for k in range(k_trunc):      

        assert(b_gam[k] != 0)
        assert(a_gam[k] != 0)
        
        exp_log_lambdas[k] = special.digamma(max(1e-30, a_gam[k])) \
                        - math.log(max(1e-30, b_gam[k]))
    
    for j in range(nb_data):
        
        assert(data[j, 0] >= 0)
        assert(data[j, 1]> 0)
        
        for k in range(k_trunc):      

                     
            exp[j, k] = data[j, 0] * exp_log_lambdas[k] - \
                (data[j, 1] * a_gam[k])/b_gam[k]
            
    return exp

@nb.jit(nopython=False, parallel=True, nogil=True)
def compute_entropy_theta(data, q_z, a_gam, b_gam):
    """
        Contribution of variational q_theta distribution to entropy
        (up to constants wrt hyperparameters Phi and variational parameters)
        
        :Parameters:
            `data` (np.array) - data
            `q_z` (([nb_data, k_trunc] np.array)) - variational approximation 
            of state distribution    
            `a_gam` (np.array) - hyperparameters: a
            `b_gam` (np.array) - hyperparameters: b 
    """
    nb_data, k_trunc = q_z.shape

    e = 0.
    # nk = q_z.sum(axis=0)
    
    for k in range(k_trunc):
        aux1 = 0. # \sum_j N_j q_{z_j}(k)
        aux2 = 0. # \sum_j y_j q_{z_j}(k)
        for j in range(nb_data):        
            y_j, n_j = data[j,]
            aux1 += n_j * q_z[j,k]
            aux2 += y_j * q_z[j,k]
        e = e - a_gam[k] * aux1 / b_gam[k] + \
            (special.digamma(max(1e-30, a_gam[k])) - \
                 math.log(max(1e-30, b_gam[k]))) * aux2
    
    return(e)
    
@nb.jit(nopython=False, parallel=True, nogil=True)
def fast_init_free_energy(data, nb_data, k_trunc, seed, s1, s2, 
                          mean_sigma, beta_z_0, nbinit):
    """
    numba implementation of free_energy initialization
    """
    nb_kmeans = 10                
    dim_data = data.shape[1]

    ratio = np.zeros((nb_data, 1), dtype=np.float64)
    ratio[:,0] = data[:,0].astype(np.float64) / data[:,1].astype(np.float64)
        
    a, b, lambdas, q_z_current = \
        k_means_init(data, ratio, nb_data, k_trunc, seed, 
                     nb_kmeans, labels=None)
    # Compute free energy for current q_z
    elbo_current = discrete_entropy(q_z_current)
    elbo_current += compute_entropy_theta(data, q_z_current, a, b)
    mean_alpha  = s1 / s2
    nk = q_z_current.sum(axis=0)
    gamma_sb = update_gamma_sb(nk, mean_alpha, mean_sigma, k_trunc)
    nb_data, k_trunc, nk, entropy = \
        compute_pi_tau_entropy(beta_z_0, q_z_current, gamma_sb)
    elbo_current += entropy
    elbo_best = elbo_current
    q_z = np.copy(q_z_current)

    for k in range(nbinit-1):
        a_c, b_c, lambdas_c, q_z_current = \
            k_means_init(data, ratio, nb_data, k_trunc, seed+k+1, 
                         nb_kmeans, labels=None)
        # Compute free energy for current q_z
        elbo_current = discrete_entropy(q_z_current)
        elbo_current += compute_entropy_theta(data, q_z_current, a_c, b_c)
        gamma_sb = update_gamma_sb(nk, mean_alpha, mean_sigma, k_trunc)
        nb_data, k_trunc, nk, entropy = \
            compute_pi_tau_entropy(beta_z_0, q_z_current, 
                                   gamma_sb)
        elbo_current += entropy
        # Keep q_z maximizing free energy / elbo
        if elbo_best < elbo_current:
            elbo_best = elbo_current
            q_z = np.copy(q_z_current)
            a = np.copy(a_c)
            b = np.copy(b_c)
            lambdas = np.copy(lambdas_c)
        
    return q_z, s1, s2, mean_sigma, beta_z_0, a, b, lambdas

def k_means_init(data, ratio, nb_data, k_trunc, seed, nbinit, labels=None):
    """
    Initialization of hyperparameters from ratio by opencv k-means
    """
    q_z, nk, labels = init_by_kmeans(ratio, nb_data, 1, k_trunc, nbinit, 
                                     seed, labels)

    lambdas = estimate_lambdas(data, q_z, nk)

    a, b  = init_a_b(lambdas, k_trunc)

    return a, b, lambdas, q_z
            
